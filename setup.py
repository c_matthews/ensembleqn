from setuptools import setup

setup(name='ensembleqn',
      version='0.1.0',
      description='EnsembleQN package',
      url='http://bitbucket.org/c_matthews/ensembleqn',
      author='C Matthews',
      author_email='c.matthews@uchicago.edu',
      license='GNU GPL3',
      packages=['ensembleqn'],
      zip_safe=False)


