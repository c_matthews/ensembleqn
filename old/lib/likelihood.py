from config import IniFile
from likelihoods.harmonic import Harmonic
from likelihoods.gmix import GMix
from likelihoods.ring import Ring
from likelihoods.lgc import LGC
from likelihoods.rosenbrock import Rosenbrock

def getlh( lhstr , ini ):
        
    if lhstr=="harmonic":
        return Harmonic(ini)

    if lhstr=="gmix":
        return GMix(ini)

    if lhstr=="ring":
        return Ring(ini)

    if lhstr=="lgc":
        return LGC(ini)

    if lhstr=="rosenbrock":
        return Rosenbrock(ini)
    
    
    raise ValueError("Unknown likelihood option in ini file: %s."%lhstr)

    return

def Likelihood( ini):
        
    lhstr = ini.get("default","likelihood").lower()
        
    return getlh(lhstr, ini )



        
        
