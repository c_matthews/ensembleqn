import numpy as np 
import scipy.linalg as sp


class LGC:
    
    def __init__( self,ini ):
        

        self.ystr = ini.get("lgc","data")
        self.gammak = ini.getfloat("lgc","gammak")
        self.gammatheta = ini.getfloat("lgc","gammatheta")
        self.mu = ini.getfloat("lgc","mu")
        self.N = ini.getint("lgc","n")


    def config(self):
        
        self.N2 = self.N*self.N
        self.y=np.loadtxt( self.ystr )        
        
        self.M = 1.0 / (self.N2 )
        self.loggammak = np.log( self.gammak )
        self.loggammatheta = np.log( self.gammatheta ) 
        
        x_r = np.linspace(0,1.0, self.N)
        y_r = x_r
        
        xs,ys = np.meshgrid( x_r, y_r )
        
        coordxy = np.zeros( (self.N2,2) )
        coordxy[:,0] = xs.flatten()
        coordxy[:,1] = ys.flatten()
        
        Dist = np.zeros( (self.N2 , self.N2  ) )
        
        for ii in np.arange(self.N2 ):
            temp = coordxy[ii,:] - coordxy
            temp=temp*temp
            Dist[ii,:] = np.sqrt( np.sum( temp,axis=1 ) ) 
        
        self.Dist = Dist
        
    def getllh(self, x, computeF=True ):
 
        V = 0
        F = np.zeros( np.size(x) )

        lvs = x[:-2]
        hps = np.exp( x[-2:] )
        loghps = x[-2:]
        
        sigmasq = hps[0]
        beta = hps[1]
        
        DistbetaN = self.Dist / ( beta * self.N )
        
        # TODO try catch
        try:
            Sigma = sigmasq * np.exp( -DistbetaN )

            SigmaInv = np.linalg.inv( Sigma )

            SigmaChol = np.linalg.cholesky( Sigma )
        except:
            return np.nan,F

        
        dSdsigmasq = Sigma
        dSdbeta = DistbetaN * Sigma
        
        TempVec = np.dot( (lvs - self.mu) , SigmaInv )
        mexplvs = self.M * np.exp( lvs )
        
        V = np.dot( self.y , lvs ) - np.sum( mexplvs ) - 0.5 * np.dot((lvs - self.mu) , TempVec )
        
        V +=  - 0.5*( 2*np.sum( np.log( np.diag( SigmaChol)   ) )  )
        V += (self.gammak -1)*loghps[0] - sigmasq/self.gammatheta - self.gammak*self.loggammatheta 
        V += (self.gammak -1)*loghps[1] - beta/self.gammatheta - self.gammak*self.loggammatheta  
        V += np.sum(  loghps )
         
        np.savetxt("s.txt", Sigma )
        np.savetxt("d.txt", self.Dist )
        


        
        if (computeF==False):
            return V,0
        
        
        # sigmasq derivative
        # Note np.sum( SigmaInv * dSdsigmasq ) = N^2
        F[-2] = -0.5* self.N2 + 0.5 * np.dot( TempVec, np.dot( dSdsigmasq , TempVec ) ) 
        F[-2] += (self.gammak - 1.0) - sigmasq / self.gammatheta +1
        
        # beta derivative
        F[-1] = -0.5* np.sum( SigmaInv * dSdbeta ) + 0.5 * np.dot( TempVec, np.dot( dSdbeta , TempVec ) ) 
        F[-1] += (self.gammak - 1.0) - beta / self.gammatheta +1
        
        F[0:-2] = self.y - mexplvs - TempVec
 
   
        
        return V,F

    def getllh_cheap(self, x, extra, computeF=True ):
 
        if (extra==[]):
            return self.getllh_expensive(x, extra) 
 
        V = 0
        F = np.zeros( np.size(x) )

        lvs = x[:-2]
        hps = np.exp( x[-2:] )
        loghps = x[-2:]
        
        sigmasq = hps[0]
        beta = hps[1]
        
        #DistbetaN = self.Dist / ( beta * self.N )
         
        SigmaInv = extra[0]
        
        #SigmaChol = np.linalg.cholesky( Sigma )
        V_chol = extra[1] 
        
        TempVec = np.dot( (lvs - self.mu) , SigmaInv )
        mexplvs = self.M * np.exp( lvs )
        
        V = np.dot( self.y , lvs ) - np.sum( mexplvs ) - 0.5 * np.dot((lvs - self.mu) , TempVec )
        
        V +=  - 0.5*(   V_chol)
        V += (self.gammak -1)*loghps[0] - sigmasq/self.gammatheta - self.gammak*self.loggammatheta 
        V += (self.gammak -1)*loghps[1] - beta/self.gammatheta - self.gammak*self.loggammatheta  
        V += np.sum(  loghps )
          
        if (computeF==False):
            return V,F , extra 
        
        F[0:-2] = self.y - mexplvs - TempVec
  
        
        return V,F, extra 


    def getllh_expensive(self, x,extra, computeF=True ):
 
        V = 0
        F = np.zeros( np.size(x) )

        lvs = x[:-2]
        hps = np.exp( x[-2:] )
        loghps = x[-2:]
        
        sigmasq = hps[0]
        beta = hps[1]
        
        DistbetaN = self.Dist / ( beta * self.N )
        
        # TODO try catch
        
        
        Sigma = sigmasq * np.exp( -DistbetaN ) 
          
        ComputeSigmaInv = True
        if len(extra)>2: 
            if (np.sum(np.abs(loghps - extra[2]))==0.0):   
                ComputeSigmaInv = False
            
        
        if (ComputeSigmaInv):
            try:
                SigmaChol = np.linalg.cholesky( Sigma )
                #SigmaCholInv = sp.solve_triangular(SigmaChol, np.eye( self.N2), lower=True ) #,check_finite=False )
                #SigmaInv = np.dot( SigmaCholInv.T , SigmaCholInv )
                V_chol = 2*np.sum( np.log( np.diag( SigmaChol)   ) )
                SigmaInv = np.linalg.inv( Sigma)
            except:
                return np.nan, F, []
        else:
            SigmaInv = extra[0]
            V_chol = extra[1] 
            
        dSdsigmasq = Sigma
        dSdbeta = DistbetaN * Sigma
        
        TempVec = np.dot( (lvs - self.mu) , SigmaInv )
        mexplvs = self.M * np.exp( lvs )
        
        V = np.dot( self.y , lvs ) - np.sum( mexplvs ) - 0.5 * np.dot((lvs - self.mu) , TempVec )
        
        V +=  - 0.5*( V_chol )
        V += (self.gammak -1)*loghps[0] - sigmasq/self.gammatheta - self.gammak*self.loggammatheta 
        V += (self.gammak -1)*loghps[1] - beta/self.gammatheta - self.gammak*self.loggammatheta  
        V += np.sum(  loghps )
         

        extra = [ np.copy(SigmaInv) , np.copy(V_chol), np.copy(loghps) ]
        
        if (computeF==False):
            return V,F,extra
        
        
        # sigmasq derivative
        # Note np.sum( SigmaInv * dSdsigmasq ) = N^2
        F[-2] = -0.5* self.N2 + 0.5 * np.dot( TempVec, np.dot( dSdsigmasq , TempVec ) ) 
        F[-2] += (self.gammak - 1.0) - sigmasq / self.gammatheta +1
        
        # beta derivative
        F[-1] = -0.5* np.sum( SigmaInv * dSdbeta ) + 0.5 * np.dot( TempVec, np.dot( dSdbeta , TempVec ) ) 
        F[-1] += (self.gammak - 1.0) - beta / self.gammatheta +1
        
        F[0:-2] = self.y - mexplvs - TempVec
 
   
        
        return V,F,extra
    
    
    
    def bail():
        
        return np.nan, 0
        
