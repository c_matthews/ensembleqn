import numpy as np


class Rosenbrock:
    
    def __init__( self,ini ):
        
        #print "Harmonic!"

        self.w = ini.getfloat("rosenbrock","w", 20)
        self.h = ini.getfloat("rosenbrock","h" , 100)


    def config(self):
        self.iw  = 1.0 / self.w
        
        
        
    def getllh(self, x, computeF=True ):
        
        X = x[0]
        Y = x[1]

        V1 = ( Y - X*X )
        V2 = ( 1.0 - X )

        V = - ( self.h * V1*V1  +  V2*V2  ) * self.iw

        if not computeF:
            return V, 0

        F = np.zeros( np.size(x) ) 

        F[0] = self.iw * ( 2*self.h* V1 * 2 * X  +  2 * V2 )
        F[1] = -self.iw * ( 2 * self.h * V1 )

        return V,F


    def getllh_cheap(self, x, extra, computeF=True ):
        V,F = self.getllh(x)
        return V,F,[0]

    
    def getllh_expensive(self, x, extra, computeF=True ):
        V,F = self.getllh(x)
        return V,F,[0]
        
