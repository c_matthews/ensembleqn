import numpy as np


class Ring:
    
    def __init__( self,ini ):
        
        #print "Harmonic!"

        self.width = ini.getfloat("ring","width", 1)
        self.amp = ini.getfloat("ring","amplitude" , 0)
        self.freq = ini.getfloat("ring","frequency" , 0)


    def config(self):
        self.iwidth = 1.0 / self.width
        
        
        
    def getllh(self, x, computeF=True ):
        
        theta = np.arctan2( x[1],x[0] )
        V1 = (np.dot(x,x) - 1.0) * self.iwidth
        V2 = np.cos( self.freq * theta )

        V = -V1*V1 - self.amp * V2*V2

        if not computeF:
            return V, 0

        F = np.zeros( np.size(x) ) 

        F = (-4*self.iwidth*V1)*x

        tmp = self.amp *self.freq*np.sin( 2*self.freq * theta ) / np.dot(x,x)

        F[0] -= tmp * x[1]
        F[1] += tmp * x[0]

        return V,F


    def getllh_cheap(self, x, extra, computeF=True ):
        V,F = self.getllh(x)
        return V,F,[0]

    
    def getllh_expensive(self, x, extra, computeF=True ):
        V,F = self.getllh(x)
        return V,F,[0]
        
