import numpy as np


class Harmonic:
    
    def __init__( self,ini ):
        
        #print "Harmonic!"

        self.OmegaStr = ini.get("harmonic","omega")


    def config(self):
        
        #print "config"

        self.Omega=np.loadtxt( self.OmegaStr )

        #print self.Omega
        
        
        
    def getllh(self, x, computeF=True ):
          
        F = - np.dot(self.Omega , x )
            
        V = 0.5 * np.dot( x , F )
        
        return V,F

    def getllh_cheap(self, x, extra, computeF=True ):
        V,F = self.getllh(x)
        return V,F,[0]

    
    def getllh_expensive(self, x, extra, computeF=True ):
        V,F = self.getllh(x)
        return V,F,[0]


        
        
