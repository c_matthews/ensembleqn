import numpy as np


class GMix:
    
    def __init__( self,ini ):
        

        self.ystr = ini.get("gmix","y")
        self.N = ini.getint("gmix","n")
        self.alpha = ini.getfloat("gmix","alpha")
        self.g = ini.getfloat("gmix","g")
        self.kappa = ini.getfloat("gmix","kappa")
        self.h = ini.getfloat("gmix","h")


    def config(self):
        

        self.y=np.loadtxt( self.ystr )

        self.R = np.max(self.y) - np.min(self.y)

        self.kappa = self.kappa / ( self.R*self.R )
        self.h = self.h * self.g / (self.alpha * self.R * self.R )

        self.M = np.mean(self.y)
        
        
    def getllh(self, x , computeF=True ):

        N = self.N

        qk = np.zeros(N)
        
        qk[0:N-1] = x[0:N-1]
        
        qk[-1] = 1.0-np.sum(qk)

        lambdak = x[N-1:2*N-1]
        muk = x[2*N-1:3*N-1]
        beta = x[-1]

        neg = 0
        neg += (qk<0).sum()
        neg += (lambdak<0).sum()
        neg += (beta<0).sum()

        if (neg>0):
            return np.nan, np.zeros( np.shape(x) )
        
        YminusMu = np.tile( self.y , (N,1) ).transpose() - muk
        xp = np.exp( -0.5*lambdak * np.power( YminusMu ,2) )
        slambdak = np.sqrt( lambdak )
        F = xp * (qk * slambdak)


        Fsum = np.sum(F,axis=1)
        FF=F

        
        V = (N * self.alpha +self.g -1) * np.log(beta) + (self.alpha-1) * np.sum( np.log( lambdak) )
        V = V - self.kappa*0.5*np.sum( np.power(muk - self.M,2) )
        V = V - beta * (self.h + np.sum(lambdak))
        
        X = np.log( np.sum(FF,axis=1) );
        
        X = np.sum(X);
        V = V + X;

        if (computeF==False):
            return V,0


        Vbeta = ( N * self.alpha + self.g - 1 ) / beta - ( self.h + np.sum(lambdak) )

        X = 0.5 * qk / slambdak
        X = X - 0.5*qk * slambdak*np.power( YminusMu ,2 )
        X = X * xp
        X = X.transpose() / Fsum

        Vlambda = (self.alpha -1) / lambdak - beta + np.sum(X,axis=1 )

        X = slambdak * xp
        X = np.transpose(X[:,0:-1]) - X[:,-1]
        X = X / Fsum

        Vq = np.sum(X,axis=1)

        #X =  -Q.* sLAMBDA.*LAMBDA.*(MU-Y).* xp ./ Fsum;
        #Vmu = -kappa*(muk-M) + sum(X')';

        X = qk * slambdak * lambdak*YminusMu 
        X = X * xp
        X = X.transpose() / Fsum

        Vmu = -self.kappa*( muk - self.M) + np.sum(X,axis=1)
        
        
            
        F = np.zeros( 3*N )
        F[0:N-1] = Vq
        F[N-1:2*N-1] = Vlambda
        F[2*N-1:3*N-1] = Vmu
        F[-1] = Vbeta
        
   
        
        return V,F

        
        
