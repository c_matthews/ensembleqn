from config import IniFile
from samplers.hmc import HMC
from samplers.hmc_gibbs import HMC_Gibbs
from samplers.verlet import Verlet
from samplers.gw  import GW
from samplers.ld  import LD
from samplers.ld_gibbs  import LD_Gibbs
from samplers.ensembleqn  import EnsembleQN
from samplers.ensembleqn_gibbs  import EnsembleQN_Gibbs

def getsampler( samplerstr , ini, likelihood, output, comm ):
      
    if samplerstr=="hmc":
        return HMC(ini, likelihood, output, comm )
      
    if samplerstr=="hmc_gibbs":
        return HMC_Gibbs(ini, likelihood, output, comm )
    
    if samplerstr=="verlet":
        return Verlet(ini, likelihood, output, comm )
    
    if samplerstr=="gw":
        return GW(ini, likelihood, output, comm )
    
    if samplerstr=="ld":
        return LD(ini, likelihood, output, comm )
    
    if samplerstr=="ld_gibbs":
        return LD_Gibbs(ini, likelihood, output, comm )
    
    if samplerstr=="eqn":
        return EnsembleQN(ini, likelihood, output, comm )
    
    if samplerstr=="eqn_gibbs":
        return EnsembleQN_Gibbs(ini, likelihood, output, comm )
    
    
    raise ValueError("Unknown likelihood option in ini file: %s."%samplerstr)

    return

def Sampler( ini, likelihood, output, comm ):
        
    samplerstr = ini.get("default","sampler").lower()
        
    return getsampler(samplerstr, ini, likelihood, output, comm )



        
        
