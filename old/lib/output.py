from config import IniFile
import numpy as np

from printinfo import print_update
import time
import datetime
import os

class Output:

    def __init__(self, ini, comm ):

        self.OutputPath = ini.get("output","path")

        self.AppendTime = ini.getboolean("output","appendtime",True)

        self.SaveX = ini.getboolean("output","savex",True)
        self.SaveA = ini.getboolean("output","savea",True)
        self.SaveV = ini.getboolean("output","savev",True)
        self.SaveTime = ini.getboolean("output","savetime",True)

        self.PrintFreq = ini.getint("output","printfreq",0)
        self.PrintTime = ini.getint("output","printtime",0)
        
        self.comm = comm


        
    
    def config(self):
 
        if self.AppendTime:
            self.OutputPath += time.strftime("_%y%m%d_%H%M%S")
        
        
        self.OutputPath = self.comm.bcast( self.OutputPath , root=0 )
        
        rank = self.comm.Get_rank()
        if (rank==0):
            print("## Output to %s"%self.OutputPath)
            try:
                os.makedirs( self.OutputPath )
            except OSError:
                if not os.path.isdir( self.OutputPath):
                    raise

        self.IDList = list()

        self.XList = list()
        self.AList = list()
        self.VList = list()
        
        self.lastprinttime = 0
        self.lastprintsteps = 0
                    
        
        
    
    def save(self):

        tt = np.array([ time.time() - self.starttime])

        for ii, id in enumerate( self.IDList):

            if (self.SaveX):
                path = self.OutputPath + "/x." + str(id)
                np.savetxt(path, self.XList[ii] )

            if (self.SaveA):
                path = self.OutputPath + "/a." + str(id)
                np.savetxt(path, self.AList[ii] )

            if (self.SaveV):
                path = self.OutputPath + "/v." + str(id)
                np.savetxt(path, self.VList[ii] )

            if (self.SaveTime): 
                path = self.OutputPath + "/ztime." + str(id)
                np.savetxt(path, tt )
                
                
            
        

    def AddOutput(self, id , X, V , A ):

        self.IDList.append( id )

        if (self.SaveX):
            self.XList.append( np.copy( X ) )

        if (self.SaveA):
            self.AList.append( A )

        if (self.SaveV):
            self.VList.append( V )
            
    def StartClock(self):
        
        self.starttime = time.time()
        
        
    def Status(self, prog , acc , V ):
        
        if   ((self.comm.Get_rank()) >0):
            return
        
        DoPrint = 0
        
        if (self.PrintTime > 0):
            if (time.time() - self.lastprinttime > self.PrintTime):
                self.lastprinttime = time.time()
                DoPrint = 1
                
        if (self.PrintFreq >0 ):
            self.lastprintsteps = self.lastprintsteps - 1
            if self.lastprintsteps < 1:
                self.lastprintsteps = self.PrintFreq
                DoPrint = 1
                
        if (DoPrint>0):    
            print_update( prog , time.time() - self.starttime , acc , V )
        
        
    
