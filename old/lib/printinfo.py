import time
import datetime

def print_header(comm):
    
    if   ((comm.Get_rank()) ==0):
    
        print "############"
        print "## Ensemble Quasi-Newton"
        print "## Test code 2016"
        print "## "
        


    comm.Barrier()
    
    print "##   Ready on %d of %d."%( comm.Get_rank(), comm.Get_size() )

    comm.Barrier()
    
def print_update(prog , tt ,  acc ,  V ):
    
    myprog = prog*100.0
    
    if prog>0:
        tr = tt * ( 1.0-prog ) / prog
    else:
        tr = 0
    
    acp = acc * 100.0
    
    
    timetaken = str(datetime.timedelta(seconds=int(tt)))
    timeleft = str(datetime.timedelta(seconds=int(tr)))
    
    print "####"
    print("# Progress: %3.1f%%    (Time: %s,   Remaining: %s)"%(myprog, timetaken,timeleft))
    print("# Acceptance  %3.2f%%        V: %f"%( acp , V ) )
