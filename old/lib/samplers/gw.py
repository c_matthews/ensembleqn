import numpy as np
from numpy import random


class GW:
    
    def __init__(self, ini, likelihood, output, comm ):
        
        self.llh = likelihood
        self.output = output
        self.comm = comm

        self.seed = ini.getint("default","seed",0)
        self.seed = self.seed + self.comm.Get_rank()
        self.Random = random.RandomState( self.seed )

        self.ICStr = ini.get("default","initialconditions")

        self.NumWalkers = ini.getint("default","Walkers")


        self.NumSamples = ini.getint("default","Samples")

        self.SampleFreq = ini.getint("default","SampleFreq",1)

        
        
        self.NumSteps = ini.getint("gw","steps",1)
        self.a = ini.getfloat("gw","factor",2.0)
        self.perturb = ini.getboolean("gw","perturb",True)
        self.locality = ini.getfloat("gw","locality",0)
        self.fluxweight = ini.getboolean("gw","fluxweight",False)
        
        
    def config(self):

        x0 = np.loadtxt( self.ICStr )

        
        x0shape = np.shape(x0)
        self.dim = np.int(x0shape[0])
        
        self.rval = np.power( self.NumWalkers , 1.0 / self.dim )

        if len( x0shape)==1:
            x0shape = np.array([ x0shape[0] , 1 ] )
            x0 = x0[:,None]
        
        if x0shape[1] < self.NumWalkers:
            x0 = np.tile( x0 , ( 1, np.int( np.ceil(self.NumWalkers/ x0shape[1])))   )

        if ( self.perturb ):
            x0 = x0 + 0.001*self.Random.randn( self.dim, np.shape(x0)[1] )


        self.MyTasks = np.arange( self.comm.Get_rank(), self.NumWalkers, self.comm.Get_size() )
        self.x0 = x0
                    

        self.g1 =  np.sqrt( self.a ) - 1.0 / np.sqrt( self.a )
        self.g2 = 1.0 / np.sqrt(self.a)
        
        self.TrajLength = np.int(np.ceil(np.float(self.NumSamples) / self.SampleFreq))        
        

    def genrand(self):

        u = self.Random.rand()

        v = self.g1*u + self.g2

        return v*v
    
    def getpc(self, x, X, locality, icC ):
         
        sz = np.shape( X )
        K = sz[1]
         
        if (locality==0):
            return np.ones( K ) 
        
        if (self.fluxweight):
            return self.getfw( x , X , icC)

        dX = X.T - x 
        dX = np.dot( icC , dX.T )
        
        r2 = np.sum( dX * dX , axis=0 ) 

        r2 = r2 - np.min(r2)
        r2 = r2 * locality
        
        r2[ r2>10 ] = 10
        
        pc = np.exp(-r2 )

        return pc / np.sum(pc)
    
    def getfw(self , xx , XX , icC):
        
        x = np.dot( icC , xx )
        X = np.dot( icC , XX )
        
        sz = np.shape( X )
        K = sz[1]
        
        ww = np.zeros( K )
        
        for ix in np.arange( K ):
            
            p = X[:,ix]
            dp = x - p
            
            st = p + dp / self.a
            en = p + dp * self.a
            
            for iy in np.arange( K ):
                
                mu = X[:,iy]
                
                ww[ix] +=  self.cleng( st, en, mu , self.rval )
                
        
        ww = ww / np.sum(ww)
        
        return ww
    
    def cleng(self , x , y , mu , r ):
        
        r2 = r*r
        
        aa = np.dot( y-x , y-x )
        bb = np.dot( y-x , x-mu )
        cc = np.dot( x-mu, x-mu) - r2
        
        disc = bb*bb - 4*aa*cc
        
        if disc<=0:
            return 0
        
        sqdisc = np.sqrt( disc )
        
        tplus = (-bb + sqdisc) / ( 2 * aa )
        if tplus>1:
            tplus=1
        if tplus<0:
            tplus=0
            
        tminus = (-bb - sqdisc)/(2*aa)
        if tminus>1:
            tminus=1
        if tminus<0:
            tminus=0
            
        l = (tplus-tminus) * np.sqrt(aa)
        
        return l
        
        
        
    
    def drawfrompc(self, pc ):
        
        cpc = np.cumsum(pc)
        
        kval = self.Random.rand() * cpc[-1]
        
        return len(pc) - np.sum( cpc>kval )
    

    def RunGW(self, x0,V0, X, N):

        sz = np.shape( X )
        K = sz[1]

        x = np.copy(x0)
        V = V0
        
        if (self.locality>0):
            C = np.cov( X )
            cC = np.linalg.cholesky( C )
            icC = np.linalg.inv( cC )
        else:
            icC = 0
        
        pc = self.getpc( x0 , X, self.locality, icC )

        a = 0

        for ii in np.arange(0,N):
        
            k = self.drawfrompc(pc)  #np.int( self.Random.rand() * sz[1] )
            xk = X[:,k]

            z = self.genrand()

            y = xk + z * ( x - xk )

            Y = self.llh.getllh(y,False)[0]
            dV = Y - V


            pc_new = self.getpc( y , X, self.locality, icC )
            
            u = self.Random.rand()

            if np.log(u) < np.log(z)*(self.dim-1) + dV + np.log( pc_new[k] /  pc[k] ):
                # Accept
                x = np.copy(y)
                V = Y
                pc = np.copy( pc_new )
                a = a + 1
            

                
        a = np.float(a) / N
        
        return x,V,a
        

        
        
    def run(self):

        X = self.x0[:,0::2]
        Y = self.x0[:,1::2]

        
        lenX = np.shape(X)[1]
        lenY = np.shape(Y)[1]
        MyTasksX = np.arange(self.comm.Get_rank(), lenX, self.comm.Get_size() )
        MyTasksY = np.arange(self.comm.Get_rank(), lenY, self.comm.Get_size() )

        lenMX = len(MyTasksX)
        lenMY = len(MyTasksY)
        
        trajxx = np.zeros( (self.dim,  self.TrajLength, lenMX ) )
        trajvx = np.zeros( (  self.TrajLength , lenMX ) )
        trajax = np.zeros( (  self.TrajLength , lenMX ) )
        
        trajxy = np.zeros( (    self.dim ,  self.TrajLength, lenMY ) )
        trajvy = np.zeros( (   self.TrajLength ,  lenMY ) )
        trajay = np.zeros( (   self.TrajLength , lenMY  ) )
        
        X = self.swapdata( X , MyTasksX )
        Y = self.swapdata( Y , MyTasksY )

        jj = 0
        acc = 0.0

        VX = np.zeros( lenMX )
        VY = np.zeros( lenMY )
        AX = np.zeros( lenMX )
        AY = np.zeros( lenMY )

        for ii,id in enumerate(MyTasksX):
            VX[ii] = self.llh.getllh( X[:,id] ,False )[0]

        for ii,id in enumerate( MyTasksY ):
            VY[ii] = self.llh.getllh( Y[:,id] ,False )[0]


        self.output.StartClock()
        
        for tt in np.arange(0, self.NumSamples):


            for ii,id in enumerate(MyTasksX):

                X[:,id],VX[ii],AX[ii] = self.RunGW( X[:,id] , VX[ii], Y, self.NumSteps )
                
            X = self.swapdata( X , MyTasksX )
            acc = acc + AX[0]

            for ii,id in enumerate(MyTasksY):

                Y[:,id],VY[ii],AY[ii] = self.RunGW( Y[:,id] , VY[ii], X , self.NumSteps )
                
            Y = self.swapdata( Y , MyTasksY )
                
            if ( np.mod(tt , self.SampleFreq)==0):
                    
                trajxx[:,jj,:] = X[:,MyTasksX]
                trajvx[jj,:] = VX
                trajax[jj,:] = AX
                trajxy[:,jj,:] = Y[:,MyTasksY]
                trajvy[jj,:] = VY
                trajay[jj,:] = AY
                jj=jj+1
                
                
            self.output.Status( np.float(tt+1)/self.NumSamples, np.float(acc)/(tt+1)  ,  VX[0] )
        
        for ii,id in enumerate(MyTasksX):
            self.output.AddOutput( 2*id , trajxx[:,:,ii], trajvx[:,ii], trajax[:,ii]  )

        for ii,id in enumerate(MyTasksY):
            self.output.AddOutput( 2*id+1 , trajxy[:,:,ii], trajvy[:,ii], trajay[:,ii]  )
                
            
    def swapdata(self, X, ids):

        newX = np.zeros( np.shape(X) )

        XX = X[:,ids]
        
        XX = self.comm.allgather( [XX , ids] )

        for x,id in XX:
            newX[:,id] = x
            
            
        return newX
        
