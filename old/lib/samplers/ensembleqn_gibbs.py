import numpy as np
from numpy import random


class EnsembleQN_Gibbs:
    
    def __init__(self, ini, likelihood, output, comm ):
        
        self.llh = likelihood
        self.output = output
        self.comm = comm

        self.seed = ini.getint("default","seed",0)
        self.seed = self.seed + self.comm.Get_rank()
        self.Random = random.RandomState( self.seed )

        self.ICStr = ini.get("default","initialconditions")

        self.NumWalkers = ini.getint("default","Walkers")

        self.NumSamples = ini.getint("default","Samples")

        self.SampleFreq = ini.getint("default","SampleFreq",1)        
        
        self.perturb = ini.getboolean("eqn_gibbs","perturb",True)
        self.UnMet = not ini.getboolean("eqn_gibbs","metropolize",True)
        self.skipdiv = not ini.getboolean("eqn_gibbs","computediv",True)
        self.rampstart = ini.getfloat("eqn_gibbs","rampstart",0.0)
        self.rampend = ini.getfloat("eqn_gibbs","rampend",self.rampstart)
        self.maxits = ini.getint("eqn_gibbs","maxits",10)
        self.maxeps = ini.getfloat("eqn_gibbs","maxeps",0.00001)
        self.gamma = ini.getfloat("eqn_gibbs","gamma",2)
        self.initialtemp = ini.getfloat("eqn_gibbs","initialtemp",1.0)
        
        
        self.NumStepsC = ini.getint("eqn_gibbs","steps_ch",1)
        self.factorC = ini.getfloat("eqn_gibbs","factor_ch",0.0)
        self.dtC = ini.getfloat("eqn_gibbs","timestep_ch",0.1)
        self.localityC = ini.getfloat("eqn_gibbs","locality_ch",0.0)
        self.localvar_stringC = ini.get("eqn_gibbs","localvars_ch","")
        self.localvar_fileC = ini.get("eqn_gibbs","localvars_ch_file","")
        self.NumStepsE = ini.getint("eqn_gibbs","steps_ex",1)
        self.factorE = ini.getfloat("eqn_gibbs","factor_ex",0.0)
        self.dtE = ini.getfloat("eqn_gibbs","timestep_ex",0.1)
        self.localityE = ini.getfloat("eqn_gibbs","locality_ex",0.0)
        self.localvar_stringE = ini.get("eqn_gibbs","localvars_ex","")
        self.localvar_fileE = ini.get("eqn_gibbs","localvars_ex_file","")
        
        self.Estart = ini.getint("eqn_gibbs","ex_start",1) -1 
        self.Eend = ini.getint("eqn_gibbs","ex_end",1) -1 
        
        
    def config(self):
 
        
        x0 = np.loadtxt( self.ICStr )
 
        x0shape = np.shape(x0)
        self.dim = np.int(x0shape[0])
         
        if len( x0shape)==1:
            x0shape = np.array([ x0shape[0] , 1 ] )
            x0 = x0[:,None]
         
        if x0shape[1] < self.NumWalkers:
            x0 = np.tile( x0 , ( 1, np.int( np.ceil(self.NumWalkers/ x0shape[1])))   )

        x0 = x0[:,0:self.NumWalkers]
 
        if ( self.perturb ):
            x0 = x0 + 0.001*self.Random.randn( self.dim, np.shape(x0)[1] )
 
        self.MyTasks = np.arange( self.comm.Get_rank(), self.NumWalkers, self.comm.Get_size() )
        self.x0 = x0 

        if self.rampend < self.rampstart:
            self.rampend = self.rampstart
            
        self.rampw = self.rampend - self.rampstart
        
        self.ramp = 1.0
          
        if (self.localvar_fileE == ""):
            self.localvarsE = [ int(x)-1 for x in self.localvar_stringE.split() ]
        else:
            lovalvarsEf = np.loadtxt( self.localvar_fileE )
            self.localvarsE = [ int(x)-1 for x in lovalvarsEf ]

          
        if (self.localvar_fileC == ""):
            self.localvarsC = [ int(x)-1 for x in self.localvar_stringC.split() ]
        else:
            lovalvarsCf = np.loadtxt( self.localvar_fileC )
            self.localvarsC = [ int(x)-1 for x in lovalvarsCf ]

         
        self.TrajLength = np.int(np.ceil(np.float(self.NumSamples) / self.SampleFreq))
        
        self.vars_e = np.arange( self.Estart, self.Eend+1)
        self.vars_c = np.setdiff1d( np.arange( 0 ,self.dim) , self.vars_e )
        
            
        
    

    def RunEQN_G(self, x0,p0,extra0, X, dt, N, forcefn, myvars, myfactor, locality, localvars):

        sz = np.shape( X )
        K = sz[1]

        
        V0,f0,extra0 = forcefn(x0,extra0)
        x = np.copy(x0)
        V = V0
        p = np.copy(p0)
        f = np.copy(f0)
        extra = [ np.array(xx) for xx in extra0 ]
        
        factor = self.ramp*myfactor

        c1 = np.exp(- self.gamma * dt )
        c3 = np.sqrt( 1 - c1*c1 )
        
        
        L = self.GetL( x  , X , False, factor, locality, localvars, myvars )[0]
        
        #np.savetxt( "L.txt", L )
        #exit()


        dt2 = dt*0.5

        H0 = np.dot(p0,p0)*0.5 - V0
        a = 0

        ExtraProb = 0.0
        
        for ii in np.arange(0,N):
            #print x

            # B
            #print p
            p[myvars] = p[myvars] + dt2*np.dot( f[myvars] , L  )
            
            # A
            if ( locality<=0 or factor<=0):
                x[myvars] = x[myvars] + dt2* np.dot( L  , p[myvars]  )
            else:
                xnew = x
                xnew[myvars] = x[myvars] + dt2* np.dot( L  , p[myvars]  )
                its = 0
                eps = 1
                Lnew = L
                while ( (its < self.maxits) and (eps > self.maxeps) ):
                    Lnew = self.GetL( xnew  , X , False, factor, locality, localvars, myvars )[0]
                    xnew_prev = xnew
                    xnew[myvars] = x[myvars]+dt2*np.dot( Lnew  , p[myvars] )
                    eps = np.sum( np.abs( xnew - xnew_prev ) )
                    its = its + 1
                    
                    
                L = Lnew
                x = xnew
                
            # (Jacobian correction)
            
            if (locality<=0 or factor<=0):
                divL = 0
                dL = 0
                J2=1
                
            else: 
                L,divL,dL = self.GetL( x  , X  ,True, factor, locality, localvars, myvars )

                dLp = np.zeros( (self.dim,self.dim) )
                xx = np.dot(dL,p[myvars])
                dLp[self.localvars,:] = xx
            
                J2 = np.linalg.det( np.eye(len(myvars)) - dt2*dLp )
                        
            # B_div
            p[myvars] = p[myvars] + dt2*divL
            
            # O
            p_pre = np.copy(p)[myvars]
            R = c3 * self.Random.randn( self.dim )[myvars]
            p[myvars] = c1 * p[myvars] +  R
            p_post = np.copy(p)[myvars]
            
            # B_div
            
            p[myvars] = p[myvars] + dt2*divL

            # (Jacobian correction part two)
            
            if (locality<=0 or factor<=0):
                divL = 0
                dL = 0
                J1=1
                
            else:  
                xx = np.dot(dL,p[myvars])
                dLp[self.localvars,:] = xx
                J1 = np.linalg.det( np.eye( len(myvars) ) + dt2*dLp )
            
            J = J1/J2
            
            if (J>0 and not np.isinf(J) and not np.isnan(J) ):
                logJ = np.log(J)
            else:
                logJ = -np.inf
            
            # A
            x[myvars] = x[myvars] + dt2*np.dot( L , p[myvars]  )
                
            V,f,extra = forcefn(x,extra)
            if ( np.isinf(V) or np.isnan(V) ):
                V = -np.inf
                break

            if not (locality<=0 or factor<=0): 
                L = self.GetL( x , X, False, factor, locality, localvars, myvars )[0]
            
            # B
            p[myvars] = p[myvars] + dt2*np.dot( f[myvars] , L  )

            p1 = np.sum( R*R ) / (2*c3*c3)
            p2 = c1 * p_post - p_pre
            p2 = np.sum( p2*p2 ) / (2*c3*c3)

            ExtraProb += p2-p1-logJ
            
        H = np.dot(p,p)*0.5 - V

        u = np.log( self.Random.rand() )

        dH = H0-H-ExtraProb



        if (self.UnMet):
            u=0
            if (np.isinf(dH) or np.isnan(dH)):
                dH = -1 # reject
            else:
                dH = 1 # accept
                
        if (u < dH ): # accept
            a=1
        else:
            a=0
            V=V0
            x=x0
            p=p0
            p[myvars]=-p0[myvars] 
            f=f0
            extra=extra0
        
        return x,p,V,a,extra
        

        
        
    def run(self):

        X = self.x0[:,0::2]
        Y = self.x0[:,1::2]

        id = self.comm.Get_rank()
        
        lenX = np.shape(X)[1]
        lenY = np.shape(Y)[1]
        MyTasksX = np.arange(self.comm.Get_rank(), lenX, self.comm.Get_size() )
        MyTasksY = np.arange(self.comm.Get_rank(), lenY, self.comm.Get_size() )


        lenMX = len(MyTasksX)
        lenMY = len(MyTasksY)
        
        trajxx = np.zeros( (self.dim,  self.TrajLength ,lenMX ) )
        trajvx = np.zeros( (  self.TrajLength , lenMX ) )
        trajax = np.zeros( (  self.TrajLength , lenMX ) )
        
        trajxy = np.zeros( (    self.dim ,  self.TrajLength , lenMY ) )
        trajvy = np.zeros( (   self.TrajLength ,  lenMY ) )
        trajay = np.zeros( (   self.TrajLength , lenMY  ) )
        
        X = self.swapdata( X , MyTasksX )
        Y = self.swapdata( Y , MyTasksY )

        jj = 0
        acc = 0.0

        VX = np.zeros( lenMX )
        VY = np.zeros( lenMY )
        AX = np.zeros( lenMX )
        AY = np.zeros( lenMY )
        PX = self.initialtemp * self.Random.randn( self.dim , lenX )
        PY = self.initialtemp * self.Random.randn( self.dim , lenY ) 
        extraX = [ [] for _ in MyTasksX ]
        extraY = [ [] for _ in MyTasksY ]


        
        self.output.StartClock()
        acc = 0.0


            
        for tt in np.arange(0, self.NumSamples):
            
            self.doramp(tt)
            
            

            for ii,id in enumerate(MyTasksX):

                X[:,id],PX[:,id],VX[ii], a1, extraX[ii] = self.RunEQN_G( X[:,id] , PX[:,id] , extraX[ii], Y, self.dtC, self.NumStepsC, self.llh.getllh_cheap, self.vars_c, self.factorC, self.localityC, self.localvarsC    )
                
                X[:,id],PX[:,id],VX[ii], a2, extraX[ii] = self.RunEQN_G( X[:,id] , PX[:,id] , extraX[ii], Y, self.dtE, self.NumStepsE, self.llh.getllh_expensive, self.vars_e, self.factorE, self.localityE, self.localvarsE   )
                 
                AX[ii] = np.min([a1,a2]) 

                if (self.comm.Get_rank()==0):
                    print [a1,a2]

                
            X = self.swapdata( X , MyTasksX )
            acc = acc + AX[0]

            for ii,id in enumerate(MyTasksY):

                Y[:,id],PY[:,id],VY[ii], a1, extraY[ii] = self.RunEQN_G( Y[:,id] , PY[:,id], extraY[ii], X , self.dtC, self.NumStepsC, self.llh.getllh_cheap, self.vars_c, self.factorC, self.localityC, self.localvarsC )
                
                Y[:,id],PY[:,id],VY[ii], a2, extraY[ii] = self.RunEQN_G( Y[:,id] , PY[:,id], extraY[ii], X , self.dtE, self.NumStepsE, self.llh.getllh_expensive, self.vars_e, self.factorE, self.localityE, self.localvarsE )
                
                AY[ii] = np.min( [a1,a2] ) 

                if (self.comm.Get_rank()==0):
                    print [a1,a2]
                
            Y = self.swapdata( Y , MyTasksY )
                
            if ( np.mod(tt , self.SampleFreq)==0):
                    
                trajxx[:,jj,:] = X[:,MyTasksX]
                trajvx[jj,:] = VX
                trajax[jj,:] = AX
                trajxy[:,jj,:] = Y[:,MyTasksY]
                trajvy[jj,:] = VY
                trajay[jj,:] = AY
                jj=jj+1
                
            self.output.Status( np.float(tt+1)/self.NumSamples, acc/(tt+1)  ,  VX[0] )


        
        for ii,id in enumerate(MyTasksX):
            print np.mean( trajax[:,ii] )
            self.output.AddOutput( 2*id , trajxx[:,:,ii], trajvx[:,ii], trajax[:,ii]  )

        for ii,id in enumerate(MyTasksY):
            #print np.mean( trajay[:,ii] )
            self.output.AddOutput( 2*id+1 , trajxy[:,:,ii], trajvy[:,ii], trajay[:,ii]  )
                
            
    def swapdata(self, X, ids):

        newX = np.zeros( np.shape(X) )

        XX = X[:,ids]
        
        XX = self.comm.allgather( [XX , ids] )

        for x,id in XX:
            newX[:,id] = x
            
            
        return newX
    
    def GetL(self, x , oldX , getdiv, factor, locality, localvars, myvars):
        
        mydim = np.size(x)
        mydimv = np.size(myvars)

        if (factor <=0): 
            return np.eye(mydimv), np.zeros( mydimv ), np.zeros( (mydimv,mydimv) )
        
        w,wp,mu,mup,X = self.GetW( x , oldX, getdiv, locality, localvars )
         
        v1 = np.sum(w)
        wn = w / v1
        
        XX = X[myvars,:]

        XX = XX.T - np.sum( XX*wn , axis=1) 
        C = np.dot( XX.T * wn , XX ) 
 
        #C = C[ np.ix_( myvars, myvars ) ]
          
        
        L = np.linalg.cholesky( np.eye( mydimv ) + ( factor) * C ) 
          
        if ((not getdiv) or ( locality <=0) or (self.skipdiv) ):
            return L,0,0

        divL = np.zeros( mydimv )
        dL = [ np.zeros( (mydimv,mydimv) )   for _ in np.arange(0,len( localvars)) ]
        
         
        print "  hi6"
        
        pC = C
        iL = np.linalg.inv( L )  

        wn2 = wn*2
        M1 = XX.T * wn2
         
        for id,ii in enumerate( localvars): 
            nw = wp[:,ii] / v1
            
            C = XX
            C1 = np.dot( C.T , (C.T * nw).T ) 
            
            M2 = mup[:,ii] 
            M2 = np.tile( M2 , (M1.shape[1],1)) 
            C2 = -np.dot( M1,M2)  
            
            C3 = -pC * np.sum( nw ) 
            
            dC = self.factor*( C1+C2+C3 )
            
            z = np.dot( dC , iL.T )
            z = np.dot( iL , z )
            z = np.tril(z ) - 0.5*np.diag(np.diag( z ) )
            dLdi = np.dot( L , z )  

            dL[id] += dLdi
            
            divL = divL + dLdi[ii,:]
            
        
        return L,divL, dL
    
    
    def GetW(self, x , X, getdiv, locality, localvars):
         

        if (locality <= 0 ):
            # Global covariance
            return np.ones( np.shape(X)[1] ),0,0,0,X
        
        xx = x[ localvars ]
        XX = X[ localvars , : ]
        
        mydim = np.size(x)

        dX = XX.T - xx

        # TODO divide by inv cov
        
        # C = np.cov( XX )
        
        invCdX = dX.T  # np.linalg.solve( C , dX.T )
        
        dw = np.sum( dX * invCdX.T, axis=1 )
        dw = dw - np.min(dw)
        
        #print dw
        
        w = np.exp( - (0.5* locality) * dw )
        mw = np.max(w)
            
        w = w / mw    

        kk = w >0.001
        X=X[:,kk]
        w=w[kk]
        dX=dX[kk,:]
        
        if ((not getdiv)  or (self.skipdiv) ):
            return w,0,0,0,X
        
        sw = np.sum(w)
        wn = w / sw
        
        wp = np.zeros( (len(w), mydim ) )
        
        mup = np.zeros( (mydim,mydim) )
        
        temp = ( locality*w) * dX.T
        wp[:, localvars ] = temp.T
        wpn = wp / sw
         

        mup[:, localvars] = np.dot( X , wpn[:, localvars ] )
 
        
        swpn = np.sum( wp, axis=0) / sw
        mu = np.sum( X*wn , axis=1) # / np.sum(w)
        
        mup = mup - np.outer( mu , swpn ) #/ sw
        
        
        
        
        return w,wp,mu,mup,X


    def doramp(self, t):

        if (t<self.rampstart):
            self.ramp = 0.0
            return

        if (self.rampw>0):
            self.ramp = ( np.float(t) - self.rampstart ) / (self.rampw)
        else:
            self.ramp = 1.0
            return

        if (self.ramp<0):
            self.ramp = 0
        if (self.ramp>1):
            self.ramp=1
            
        
