import numpy as np
from numpy import random


class LD:
    
    def __init__(self, ini, likelihood, output, comm ):
        
        self.llh = likelihood
        self.output = output
        self.comm = comm

        self.seed = ini.getint("default","seed",0)
        self.seed = self.seed + self.comm.Get_rank()
        self.Random = random.RandomState( self.seed )

        self.ICStr = ini.get("default","initialconditions")

        self.NumWalkers = ini.getint("default","Walkers")


        self.NumSamples = ini.getint("default","Samples")

        self.SampleFreq = ini.getint("default","SampleFreq",1)
        
        self.NumSteps = ini.getint("ld","steps",50)
        self.dt = ini.getfloat("ld","timestep",0.1)
        self.gamma = ini.getfloat("ld","gamma",2)
        self.kfactor = ini.getfloat("ld","kfactor",1.0)
        self.UnMet = not ini.getboolean("ld","metropolize",True)
        
        
    def config(self):

        x0 = np.loadtxt( self.ICStr )

        
        x0shape = np.shape(x0)
        self.dim = np.int(x0shape[0])
        

        if len( x0shape)==1:
            x0shape = np.array([ x0shape[0] , 1 ] )
            x0 = x0[:,None]
        
        if x0shape[1] < self.NumWalkers:
            x0 = np.tile( x0 , ( 1, np.int( np.ceil(self.NumWalkers/ x0shape[1])))   )



        self.MyTasks = np.arange( self.comm.Get_rank(), self.NumWalkers, self.comm.Get_size() )
        self.x0 = x0

        self.c1 = np.exp(-self.dt * self.gamma )

        self.c3 = np.sqrt( 1- self.c1*self.c1)

        self.TrajLength = np.int(np.ceil(np.float(self.NumSamples) / self.SampleFreq))
                    



    def RunLD(self, x0,p0,V0,f0, dt, N):

        x = np.copy(x0)
        f = np.copy(f0)
        p = np.copy(p0)

        dt2 = dt*0.5

        H0 =self.kfactor*np.dot(p0,p0)*0.5 - V0
        
        ExtraProb = 0.0
        
        for ii in np.arange(0,N):

            # B
            p = p + dt2*f
            # A
            x = x + dt2*p
            # O
            p_pre = np.copy(p)
            R = self.c3 * self.Random.randn( self.dim )
            p = self.c1 * p +  R
            p_post = np.copy(p)
            # A
            x = x + dt2*p
            V,f = self.llh.getllh(x)
            if ( np.isinf(V) or np.isnan(V) ):
                V = -np.inf
                break
            # B
            p = p + dt2*f

            p1 = np.sum( R*R ) / (2*self.c3*self.c3)
            p2 = self.c1 * p_post - p_pre
            p2 = np.sum( p2*p2 ) / (2*self.c3*self.c3)

            ExtraProb += p2-p1
            



        H = self.kfactor*np.dot(p,p)*0.5 - V

        u = np.log( self.Random.rand() )

        dH = H0-H-ExtraProb

        if (self.UnMet):
            u=0
            if (np.isinf(dH) or np.isnan(dH)):
                dH = -1 # reject
            else:
                dH = 1 # accept
                
                
        if (u < dH ): # accept
            a=1
        else:
            a=0
            V=V0
            x=x0
            p=-p0
            f=f0
        
        return x,p,V,f,a
        

        
        
    def run(self):

        self.output.StartClock()
            
        acc_st = 0.0
        ii_st = 0
        Tot_st = len(self.MyTasks) * self.NumSamples 
        
        for task in self.MyTasks:

            trajx = np.zeros( (self.dim ,  self.TrajLength ) )
            trajv = np.zeros( (1 ,  self.TrajLength ) )
            traja = np.zeros( (1  ,  self.TrajLength ) )
            
            jj=0

            x = self.x0[:,task]
            p = self.Random.randn( self.dim )

            acc = 0.0

            V,f = self.llh.getllh(x)

            for ii in np.arange(0, self.NumSamples):

                x,p,V,f,a = self.RunLD( x , p , V , f , self.dt , self.NumSteps )

                acc = acc + a
                
                if ( np.mod(ii , self.SampleFreq)==0):
                    
                    trajx[:,jj] = x
                    trajv[:,jj] = V
                    traja[:,jj] = a
                    jj=jj+1
                    
                ii_st = ii_st + 1
                acc_st = acc_st + a
                
                self.output.Status( np.float(ii_st+1)/Tot_st, acc_st/(ii_st)  ,  V )

            acc = acc / self.NumSamples
            print acc

            self.output.AddOutput( task , trajx, trajv, traja  )
            
            
        
