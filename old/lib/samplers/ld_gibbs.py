import numpy as np
from numpy import random


class LD_Gibbs:
    
    def __init__(self, ini, likelihood, output, comm ):
        
        self.llh = likelihood
        self.output = output
        self.comm = comm

        self.seed = ini.getint("default","seed",0)
        self.seed = self.seed + self.comm.Get_rank()
        self.Random = random.RandomState( self.seed )

        self.ICStr = ini.get("default","initialconditions")

        self.NumWalkers = ini.getint("default","Walkers")


        self.NumSamples = ini.getint("default","Samples")

        self.SampleFreq = ini.getint("default","SampleFreq",1) 
         
        self.NumStepsC = ini.getint("ld_gibbs","steps_ch",50)
        self.dtC = ini.getfloat("ld_gibbs","timestep_ch",0.1)
        self.NumStepsE = ini.getint("ld_gibbs","steps_ex",50)
        self.dtE = ini.getfloat("ld_gibbs","timestep_ex",0.1)
        self.Estart = ini.getint("ld_gibbs","ex_start",1) -1 
        self.Eend = ini.getint("ld_gibbs","ex_end",1) -1 
        self.gamma = ini.getfloat("ld_gibbs","gamma",2)
        self.kfactor = ini.getfloat("ld_gibbs","kfactor",1.0)
        self.UnMet = not ini.getboolean("ld_gibbs","metropolize",True)
        
        
    def config(self):

        x0 = np.loadtxt( self.ICStr )

        
        x0shape = np.shape(x0)
        self.dim = np.int(x0shape[0])
        

        if len( x0shape)==1:
            x0shape = np.array([ x0shape[0] , 1 ] )
            x0 = x0[:,None]
        
        if x0shape[1] < self.NumWalkers:
            x0 = np.tile( x0 , ( 1, np.int( np.ceil(self.NumWalkers/ x0shape[1])))   )



        self.MyTasks = np.arange( self.comm.Get_rank(), self.NumWalkers, self.comm.Get_size() )
        self.x0 = x0 

        self.TrajLength = np.int(np.ceil(np.float(self.NumSamples) / self.SampleFreq))
        
        self.vars_e = np.arange( self.Estart, self.Eend+1)
        self.vars_c = np.setdiff1d( np.arange( 0 ,self.dim) , self.vars_e )
                    



    def RunLD_G(self, x0,p0,extra0, dt, N, forcefn, myvars):

        V0,f0,extra0 = forcefn(x0,extra0)
        x = np.copy(x0) 
        p = np.copy(p0)
        f = np.copy(f0)
        V=V0
        extra = [ np.array(xx) for xx in extra0 ]

        dt2 = dt*0.5
        c1 = np.exp(-self.gamma * dt )
        c3 = np.sqrt(1-c1*c1)

        #print p0
        #print V0

        H0 =self.kfactor*np.dot(p0,p0)*0.5 - V0
        
        ExtraProb = 0.0
        
        for ii in np.arange(0,N):

            # B
            p[myvars] = p[myvars] + dt2*f[myvars]
            # A
            x[myvars] = x[myvars] + dt2*p[myvars]
            # O
            p_pre = np.copy(p)[myvars]
            R = c3 * self.Random.randn( self.dim )[myvars]
            p[myvars] = c1 * p[myvars] +  R 
            p_post = np.copy(p)[myvars]
            # A
            x[myvars] = x[myvars] + dt2*p[myvars]
            V,f,extra = forcefn(x,extra)
            if ( np.isinf(V) or np.isnan(V) ):
                V = -np.inf
                break
            # B
            p[myvars] = p[myvars] + dt2*f[myvars]

            if (c3>0):
                p1 = np.sum( R*R ) / (2*c3*c3)
                p2 = c1 * p_post - p_pre
                p2 = np.sum( p2*p2 ) / (2*c3*c3)

                ExtraProb += p2-p1
               



        H = self.kfactor*np.dot(p,p)*0.5 - V

        u = np.log( self.Random.rand() )

        dH = H0-H-ExtraProb

        if (self.UnMet):
            u=0
            if (np.isinf(dH) or np.isnan(dH)):
                dH = -1 # reject
            else:
                dH = 1 # accept
                
                
        if (u < dH ): # accept
            a=1
        else:
            a=0
            V=V0
            x=x0
            p=p0
            p[myvars]=-p0[myvars] 
            extra = extra0
        
        return x,p,V,a,extra
        

        
        
    def run(self):

        self.output.StartClock()
            
        acc_st = 0.0
        ii_st = 0
        Tot_st = len(self.MyTasks) * self.NumSamples 
        
        for task in self.MyTasks:

            trajx = np.zeros( (self.dim ,  self.TrajLength ) )
            trajv = np.zeros( (1 ,  self.TrajLength ) )
            traja = np.zeros( (1  ,  self.TrajLength ) )
            
            jj=0

            x = self.x0[:,task]
            p = self.Random.randn( self.dim )

            acc = 0.0
            
            extra = [] 

            for ii in np.arange(0, self.NumSamples):

                x,p,V,a1, extra = self.RunLD_G( x , p , extra , self.dtC , self.NumStepsC, self.llh.getllh_cheap, self.vars_c )
                
                x,p,V,a2, extra = self.RunLD_G( x , p , extra , self.dtE , self.NumStepsE, self.llh.getllh_expensive, self.vars_e )

                a = np.min( [a1,a2] )
                acc = acc + a
                
                if ( np.mod(ii , self.SampleFreq)==0):
                    
                    trajx[:,jj] = x
                    trajv[:,jj] = V
                    traja[:,jj] = a
                    jj=jj+1
                    
                ii_st = ii_st + 1
                acc_st = acc_st + a
                
                self.output.Status( np.float(ii_st+1)/Tot_st, acc_st/(ii_st)  ,  V )

            acc = acc / self.NumSamples
            print acc

            self.output.AddOutput( task , trajx, trajv, traja  )
            
            
        
