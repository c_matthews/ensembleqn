import numpy as np
from numpy import random


class EnsembleQN:
    
    def __init__(self, ini, likelihood, output, comm ):
        
        self.llh = likelihood
        self.output = output
        self.comm = comm

        self.seed = ini.getint("default","seed",0)
        self.seed = self.seed + self.comm.Get_rank()
        self.Random = random.RandomState( self.seed )

        self.ICStr = ini.get("default","initialconditions")

        self.NumWalkers = ini.getint("default","Walkers")

        self.NumSamples = ini.getint("default","Samples")

        self.SampleFreq = ini.getint("default","SampleFreq",1)        
        
        self.NumSteps = ini.getint("eqn","steps",1)
        self.perturb = ini.getboolean("eqn","perturb",True)
        self.factor = ini.getfloat("eqn","factor",0.0)
        self.dt = ini.getfloat("eqn","timestep",0.1)
        self.gamma = ini.getfloat("eqn","gamma",2)
        self.UnMet = not ini.getboolean("eqn","metropolize",True)
        self.skipdiv = not ini.getboolean("eqn","computediv",True)
        self.initialtemp = ini.getfloat("eqn","initialtemp",1.0)
        self.rampstart = ini.getfloat("eqn","rampstart",0.0)
        self.rampend = ini.getfloat("eqn","rampend",self.rampstart)
        self.locality = ini.getfloat("eqn","locality",0.0)
        self.localvar_string = ini.get("eqn","localvars","0")
        self.maxits = ini.getint("eqn","maxits",10)
        self.maxeps = ini.getfloat("eqn","maxeps",0.00001)
        
        
    def config(self):
 
        
        x0 = np.loadtxt( self.ICStr )
 
        x0shape = np.shape(x0)
        self.dim = np.int(x0shape[0])
         
        if len( x0shape)==1:
            x0shape = np.array([ x0shape[0] , 1 ] )
            x0 = x0[:,None]
         
        if x0shape[1] < self.NumWalkers:
            x0 = np.tile( x0 , ( 1, np.int( np.ceil(self.NumWalkers/ x0shape[1])))   )

        x0 = x0[:,0:self.NumWalkers]
 
        if ( self.perturb ):
            x0 = x0 + 0.001*self.Random.randn( self.dim, np.shape(x0)[1] )
 
        self.MyTasks = np.arange( self.comm.Get_rank(), self.NumWalkers, self.comm.Get_size() )
        self.x0 = x0 
        self.c1 = np.exp(- self.gamma * self.dt )
        self.c3 = np.sqrt( 1 - self.c1*self.c1 )

        if self.rampend < self.rampstart:
            self.rampend = self.rampstart
            
        self.rampw = self.rampend - self.rampstart
        
        self.ramp = 1.0
        
        if (self.locality<=0):
            self.UseGlobal = True
        else:
            self.UseGlobal = False
          
        
        self.localvars = [ int(x)-1 for x in self.localvar_string.split() ]
         
        
        self.TrajLength = np.int(np.ceil(np.float(self.NumSamples) / self.SampleFreq))
        
            
        
    

    def RunEQN(self, x0,p0,V0,f0, X, N):

        sz = np.shape( X )
        K = sz[1]

        x = np.copy(x0)
        V = V0
        p = np.copy(p0)
        f = np.copy(f0)

        
        
        L = self.GetL( x , X, False )[0]        

        dt2 = self.dt*0.5

        H0 = np.dot(p0,p0)*0.5 - V0
        a = 0

        ExtraProb = 0.0
        
        for ii in np.arange(0,N):
            #print x

            # B
            #print p
            p = p + dt2*np.dot( f , L )
            
            # A
            if (self.UseGlobal or self.ramp*self.factor<=0):
                x = x + dt2* np.dot( L , p  )
            else:
                xnew = x + dt2* np.dot( L , p  )
                its = 0
                eps = 1
                Lnew = L
                while ( (its < self.maxits) and (eps > self.maxeps) ):
                    Lnew = self.GetL( xnew , X, False )[0]
                    xnew_prev = xnew
                    xnew = x+dt2*np.dot( Lnew , p )
                    eps = np.sum( np.abs( xnew - xnew_prev ) )
                    its = its + 1
                if ((its >= self.maxits)): 
                    V = -np.inf
                    break
                    
                    
                L = Lnew
                x = xnew
                
            # (Jacobian correction)
            
            if (self.UseGlobal or self.ramp*self.factor<=0):
                divL = 0
                dL = 0
                J2 = 1
            else:
                L,divL,dL = self.GetL( x , X,True ) 
                dLp = np.zeros( (self.dim,self.dim) )
                xx = np.dot(dL,p)
                dLp[self.localvars,:] = xx
                J2 = np.linalg.det( np.eye(self.dim) + dt2*dLp )
                        
            # B_div
            p = p + dt2*divL
            
            # O
            p_pre = p
            R = self.c3 * self.Random.randn( self.dim )
            p = self.c1 * p +  R
            p_post = p
            
            # B_div
            
            p = p + dt2*divL

            # (Jacobian correction part two)
            
            if (self.UseGlobal or self.ramp*self.factor<=0):
                divL = 0
                dL = 0
                J1 = 1
            else:
                xx = np.dot(dL,p)
                dLp[self.localvars,:] = xx
                J1 = np.linalg.det( np.eye(self.dim) - dt2*dLp )
            
            J = J2/J1
            
            if (J>0 and not np.isinf(J) and not np.isnan(J) ):
                logJ = np.log(J)
            else:
                logJ = -np.inf
            
            # A
            x = x + dt2*np.dot( L , p  )
                
            V,f = self.llh.getllh(x)
            if ( np.isinf(V) or np.isnan(V) ):
                V = -np.inf
                break
            
            L = self.GetL( x , X, False )[0]
            
            # B
            p = p + dt2*np.dot( f , L  )

            p1 = np.sum( R*R ) / (2*self.c3*self.c3)
            p2 = self.c1 * p_post - p_pre
            p2 = np.sum( p2*p2 ) / (2*self.c3*self.c3)

            ExtraProb += p2-p1-logJ
            
        H = np.dot(p,p)*0.5 - V

        u = np.log( self.Random.rand() )

        dH = H0-H-ExtraProb



        if (self.UnMet):
            u=0
            if (np.isinf(dH) or np.isnan(dH) ):
                dH = -1 # reject
            else:
                dH = 1 # accept
                
        if (u < dH ): # accept
            a=1
        else:
            a=0
            V=V0
            x=x0
            p=-p0
            f=f0
        
        return x,p,V,f,a 
        

        
        
    def run(self):

        X = self.x0[:,0::2]
        Y = self.x0[:,1::2]

        
        lenX = np.shape(X)[1]
        lenY = np.shape(Y)[1]
        MyTasksX = np.arange(self.comm.Get_rank(), lenX, self.comm.Get_size() )
        MyTasksY = np.arange(self.comm.Get_rank(), lenY, self.comm.Get_size() )

        lenMX = len(MyTasksX)
        lenMY = len(MyTasksY)
        
        trajxx = np.zeros( (self.dim,  self.TrajLength ,lenMX ) )
        trajvx = np.zeros( (  self.TrajLength , lenMX ) )
        trajax = np.zeros( (  self.TrajLength , lenMX ) )
        
        trajxy = np.zeros( (    self.dim ,  self.TrajLength , lenMY ) )
        trajvy = np.zeros( (   self.TrajLength ,  lenMY ) )
        trajay = np.zeros( (   self.TrajLength , lenMY  ) )
        
        X = self.swapdata( X , MyTasksX )
        Y = self.swapdata( Y , MyTasksY )

        jj = 0
        acc = 0.0

        VX = np.zeros( lenMX )
        VY = np.zeros( lenMY )
        AX = np.zeros( lenMX )
        AY = np.zeros( lenMY )
        PX = self.initialtemp * self.Random.randn( self.dim , lenX )
        PY = self.initialtemp * self.Random.randn( self.dim , lenY )
        FX = np.zeros( np.shape( X ) )
        FY = np.zeros( np.shape( Y ) )

        for ii,id in enumerate(MyTasksX):
            VX[ii], FX[:,id] = self.llh.getllh( X[:,id] )

        for ii,id in enumerate( MyTasksY ):
            VY[ii], FY[:,id] = self.llh.getllh( Y[:,id] )


        
        self.output.StartClock()
        acc = 0.0


            
        for tt in np.arange(0, self.NumSamples):
            
            self.doramp(tt)
            
            

            for ii,id in enumerate(MyTasksX):

                X[:,id],PX[:,id],VX[ii], FX[:,id] ,AX[ii] = self.RunEQN( X[:,id] , PX[:,id] , VX[ii], FX[:,id], Y, self.NumSteps )
                
            X = self.swapdata( X , MyTasksX )
            acc = acc + AX[0]

            for ii,id in enumerate(MyTasksY):

                Y[:,id],PY[:,id],VY[ii],FY[:,id], AY[ii] = self.RunEQN( Y[:,id] , PY[:,id], VY[ii], FY[:,id], X , self.NumSteps )
                
            Y = self.swapdata( Y , MyTasksY )
                
            if ( np.mod(tt , self.SampleFreq)==0):
                    
                trajxx[:,jj,:] = X[:,MyTasksX]
                trajvx[jj,:] = VX
                trajax[jj,:] = AX
                trajxy[:,jj,:] = Y[:,MyTasksY]
                trajvy[jj,:] = VY
                trajay[jj,:] = AY
                jj=jj+1
                
            self.output.Status( np.float(tt+1)/self.NumSamples, acc/(tt+1)  ,  VX[0] )


        
        for ii,id in enumerate(MyTasksX):
            print np.mean( trajax[:,ii] )
            self.output.AddOutput( 2*id , trajxx[:,:,ii], trajvx[:,ii], trajax[:,ii]  )

        for ii,id in enumerate(MyTasksY):
            print np.mean( trajay[:,ii] )
            self.output.AddOutput( 2*id+1 , trajxy[:,:,ii], trajvy[:,ii], trajay[:,ii]  )
                
            
    def swapdata(self, X, ids):

        newX = np.zeros( np.shape(X) )

        XX = X[:,ids]
        
        XX = self.comm.allgather( [XX , ids] )

        for x,id in XX:
            newX[:,id] = x
            
            
        return newX
    
    def GetL(self, x , X, getdiv):
                
        if (self.ramp*self.factor <=0): 
            return np.eye(self.dim), np.zeros( self.dim ), np.zeros( (self.dim,self.dim) )
         
        
        w,wp,mu,mup,X = self.GetW( x , X, getdiv )
        
        

        #print w[ w>0.01 ]
        #print X
        #exit()
        # Need numpy 1.10
        #C = np.cov( X ,y=None,rowvar=1, bias=0, ddof=None, fweights=None,  aweights=w )

        v1 = np.sum(w)
        wn = w / v1
        
        #v2 = np.sum( w*w )

        XX = X.T - np.sum( X*wn , axis=1)# / v1
        C = np.dot( XX.T * wn , XX ) #/ v1 # * (v1 / ( v1*v1 - v2 ) )
 
        try:
            L = np.linalg.cholesky( np.eye( self.dim ) + (self.ramp*self.factor) * C ) 
        except:
            L = np.eye( self.dim )
    
        divL = np.zeros( self.dim )
        dL = [ np.zeros( (self.dim,self.dim) )   for _ in np.arange(0,len(self.localvars)) ]
        
        if ((not getdiv) or (self.locality <=0) or (self.skipdiv) ):
            return L,divL,dL
        
        
        pC = C
        iL = np.linalg.inv( L )
        #iL = np.linalg.solve( L , np.eye( 9 ) )
        
        #print iL
        #return np.eye(self.dim), np.zeros( self.dim ), np.zeros( (self.dim,self.dim) )
        
        #wn = w / np.sum(w)
        wn2 = wn*2
        M1 = XX.T * wn2
        #return np.eye(self.dim), np.zeros( self.dim ), np.zeros( (self.dim,self.dim) )
        
        for id,ii in enumerate(self.localvars): 
            nw = wp[:,ii] / v1
            
            C = XX
            C1 = np.dot( C.T , (C.T * nw).T ) 
            
            M2 = mup[:,ii] 
            M2 = np.tile( M2 , (M1.shape[1],1)) 
            C2 = -np.dot( M1,M2)  
            
            C3 = -pC * np.sum( nw ) 
            
            dC = self.factor*( C1+C2+C3 )
            
            z = np.dot( dC , iL.T )
            z = np.dot( iL , z )
            z = np.tril(z ) - 0.5*np.diag(np.diag( z ) )
            dLdi = np.dot( L , z ) 

            #print (ii, np.shape(dL), np.shape(dLdi))
            #dL.append( dLdi[:,:,id] =  dLdi

            dL[id] += dLdi
            
            divL = divL + dLdi[ii,:]
             
        
        return L,divL, dL
    
    
    def GetW(self, x , X, getdiv):
        
        if (self.UseGlobal):
            # Global covariance
            return np.ones( np.shape(X)[1] )
        
        xx = x[ self.localvars ]
        XX = X[ self.localvars , : ]
        
        dX = XX.T - xx

        # TODO divide by inv cov
        
        # C = np.cov( XX )
        
        invCdX = dX.T  # np.linalg.solve( C , dX.T )
        
        dw = np.sum( dX * invCdX.T, axis=1 )
        dw = dw - np.min(dw)
        
        #print dw
        ww = - (0.5*self.locality) * dw

        kk = ww >-7 #0.001
        ww=ww[kk]
        X=X[:,kk]
        w=np.exp( ww  )
        dX=dX[kk,:]

        #w = np.exp( - (0.5*self.locality) * dw  )
        mw = np.max(w)
            
        w = w / mw    

        
        if ((not getdiv) or (self.locality <=0) or (self.skipdiv) ):
            return w,0,0,0,X
        
        sw = np.sum(w)
        wn = w / sw
        
        wp = np.zeros( (len(w), self.dim ) )
        
        mup = np.zeros( (self.dim,self.dim) )
        
        temp = (self.locality*w) * dX.T
        wp[:,self.localvars ] = temp.T
        wpn = wp / sw
        
        #for ii in self.localvars:
        #    mup[:,ii] = np.sum( wpn[:,ii] * X, axis=1  ) # / sw

        mup[:,self.localvars] = np.dot( X , wpn[:,self.localvars ] )

        #print mup[:,self.localvars]

        #xx=  np.dot( X , (wpn[:,self.localvars ])  )
        #xx = np.sum(  X.T  *   wpn[:,self.localvars] , axis=1)
        #print xx

        #exit()
        
        swpn = np.sum( wp, axis=0) / sw
        mu = np.sum( X*wn , axis=1) # / np.sum(w)
        
        mup = mup - np.outer( mu , swpn ) #/ sw
        
        
        
        
        return w,wp,mu,mup,X


    def doramp(self, t):

        if (t<self.rampstart):
            self.ramp = 0.0
            return

        if (self.rampw>0):
            self.ramp = ( np.float(t) - self.rampstart ) / (self.rampw)
        else:
            self.ramp = 1.0
            return

        if (self.ramp<0):
            self.ramp = 0
        if (self.ramp>1):
            self.ramp=1
            
        
