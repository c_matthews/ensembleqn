import numpy as np
from numpy import random


class Verlet:
    
    def __init__(self, ini, likelihood, output, comm ):
        
        self.llh = likelihood
        self.output = output
        self.comm = comm

        self.seed = ini.getint("default","seed",0)
        self.seed = self.seed + self.comm.Get_rank()
        self.Random = random.RandomState( self.seed )

        self.ICStr = ini.get("default","initialconditions")

        self.NumWalkers = ini.getint("default","Walkers")


        self.NumSamples = ini.getint("default","Samples")

        self.SampleFreq = ini.getint("default","SampleFreq",1)
        
        self.dt = ini.getfloat("verlet","timestep",0.1)
        
        
    def config(self):

        x0 = np.loadtxt( self.ICStr )

        
        x0shape = np.shape(x0)
        self.dim = np.int(x0shape[0])
        

        if len( x0shape)==1:
            x0shape = np.array([ x0shape[0] , 1 ] )
            x0 = x0[:,None]
        
        if x0shape[1] < self.NumWalkers:
            x0 = np.tile( x0 , ( 1, np.int( np.ceil(self.NumWalkers/ x0shape[1])))   )



        self.MyTasks = np.arange( self.comm.Get_rank(), self.NumWalkers, self.comm.Get_size() )
        self.x0 = x0
                    
        self.TrajLength = np.int(np.ceil(np.float(self.NumSamples) / self.SampleFreq))



    def VStep(self, x0,V0,f0,p0, dt):
  
 
        dt2 = dt*0.5
        
        p = p0 + dt2*f0
        x = x0 + dt*p
        V,f = self.llh.getllh(x)
        p = p + dt2*f

        V = np.dot(p,p)*0.5 - V
        
        return x,V,f,p
        

        
        
    def run(self):
        
        self.output.StartClock()
            
        acc_st = 0.0
        ii_st = 0
        Tot_st = len(self.MyTasks) * self.NumSamples

        for task in self.MyTasks:

            trajx = np.zeros( (self.dim ,  self.TrajLength ) )
            trajv = np.zeros( (1 ,  self.TrajLength ) )
            traja = np.zeros( (1  ,  self.TrajLength ) )
            
            jj=0

            x = self.x0[:,task]

            acc = 0.0

            V,f = self.llh.getllh(x)
            p = self.Random.randn( self.dim )

            for ii in np.arange(0, self.NumSamples):

                x,V,f,p = self.VStep( x , V , f,p , self.dt )

                
                if ( np.mod(ii , self.SampleFreq)==0):
                    
                    trajx[:,jj] = x
                    trajv[:,jj] = V
                    traja[:,jj] = 1
                    jj=jj+1
                    
                ii_st = ii_st + 1
                acc_st = acc_st + 1
                
                self.output.Status( np.float(ii_st+1)/Tot_st, acc_st/(ii_st)  ,  V )

            acc = acc / self.NumSamples
           

            self.output.AddOutput( task , trajx, trajv, traja  )
            
            
        
