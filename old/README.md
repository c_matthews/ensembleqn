Ensemble Quasi-Newton (old)
==============

Installation
--------------

Simply copy the files into a directory and run.

Execution
--------------

You can run the package by executing

    mpirun ./ensqn [file]

where *file* is a .ini file giving the parameters of the run. Examples are given in the `examples` directory.

A quick example to test that the package works correctly is:

    mpirun ./ensqn examples/Oscillator/run_ld.ini

will run Metropolized Langevin dynamics on a harmonic oscillator example. Looking in the file, it runs using 64 parallel walkers, for 1000 samples. Each sample runs for 50 steps, with a timestep of 0.5.



Extending the package
--------------

Likelihoods and Samplers can be easily added to the package as the code is very simple. For example, imagine we wished to add a double-well example, we would do the following:

* Copy `lib/likelihoods/harmonic.py` and rename it to `lib/likelihoods/dw.py`. This will be our new likelihood definiton file.
* In our new `dw.py` file, load any required parameters into the `_init_` function, and any work needed for setup can be done in the `config` function. We also need to change the name of the class to avoid conflicts, and actually put in the likelihood equation itself. The file could be the following:
~~~~
class Dwell:
    
    def __init__( self,ini ):
	
        # Load in the height of the barrier, default is 1.0

        self.height = ini.getfloat("doublewell","myheight",1.0)

    def config(self):
	
        # We don't need to do any setup here as it's pretty simple

        pass
	
    def getllh(self, x, computeF=True ):

        # Now the actual force and likelihoods
	  
        F = - self.height * ( x * x - 1.0 ) * x
        V = 0.25 * self.height * ( x * x - 1.0 ) * ( x * x - 1.0 )
	
        return V,F

    # These final two routines can left as they are

    def getllh_cheap(self, x, extra, computeF=True ):
        V,F = self.getllh(x)
        return V,F,[0]

    def getllh_expensive(self, x, extra, computeF=True ):
        V,F = self.getllh(x)
        return V,F,[0]
~~~~

* Now that the code is written, we can add it in to the manifest file. Open up `lib/likelihood.py` and add in `from likelihoods.dw import Dwell` to the start of the file, and 
~~~~
    if lhstr=="doublewell":
        return Dwell(ini)
~~~~
to the `getlh` function. 

* Then that's it! You're done! The double-well example can be used in the input files.

A similar procedure can be used to add in new samplers.

Help
---------------

Please sent any bugs and difficulties to c.matthews@uchicago.edu, or see the accompanying document at https://arxiv.org/abs/1607.03954 .
