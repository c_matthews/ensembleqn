#
# Samples a badly scaled 2d Gaussian using EnsembleQN sampling  
#
# We compare Langevin dynamics to EnsembleQN with a global preconditioning matrix 
#
#
# Usage:
# > mpirun -np N python example1.py
# where "N" is the number of cores to run on, e.g. 4
# 
import ensembleqn.ensembleqn as eqn
import numpy as np
import acor  
import time
from mpi4py import MPI

if (MPI.COMM_WORLD.Get_rank()==0):
    master=True
else:
    master=False
    
#
# Sample a 2D Gaussian
#
# Define the log probability function:
# 
global icov
global mu
icov =  np.array([ [50,49] , [49,50] ])
mu = np.reshape(np.array([0.5,-0.25]),(2,1) )

def log_prob_fn(p  ):
    pp = p - mu 
    lpf = - 0.5 * np.dot(pp.T , np.dot( icov, pp)  )
    return lpf

def grad_log_prob_fn( p  ):
    glpf = - np.dot( icov, p - mu)
    return glpf
#
# Now define the function that gets the fast and slow IATs using ACOR
#
def get_iat( q ):
    
    # q has dimension [ 2 , walkers , steps ]
    # we look at the IAT of the ensemble averages in the fast and slow direction
     
    x = np.squeeze( q[0,:,:] )
    y = np.squeeze( q[1,:,:] ) 
    
    qfast = np.mean( x+y,axis=0 )
    qslow = np.mean( x-y,axis=0 )  
    
    try:
        tauF, mF, sigF = acor.acor( qfast )
    except:
        tauF, mF, sigF = 1.0,0,0 # Error!
        
    try:
        tauS, mS, sigS = acor.acor( qslow )
    except:
        tauS, mS, sigS = 1.0,0,0 # Error!
    
    return tauF, tauS , mF,mS,sigF,sigS
#
# Now create the  sampler objects
# We use 32 walkers in this example 
#
if (master):
    print "Creating sampling objects..."

if (master):
    print " > Metropolized Langevin dynamics"
MLDsampler = eqn.Sampler( log_prob_fn , grad_log_prob_fn , nwalkers=32, ic=[0,0]  )

if (master):
    print " > un-Metropolized Langevin dynamics"
uMLDsampler = eqn.Sampler( log_prob_fn , grad_log_prob_fn , nwalkers=32, ic=[0,0]  )

if (master):
    print " > Metropolized EQN dynamics"
MEQNsampler = eqn.Sampler( log_prob_fn , grad_log_prob_fn , nwalkers=32, ic=[0,0]  )

if (master):
    print " > un-Metropolized EQN dynamics"
uMEQNsampler = eqn.Sampler( log_prob_fn , grad_log_prob_fn , nwalkers=32, ic=[0,0]  )

if (master):
    print ""
    print "Now doing 1000 steps of burn-in"

MLDsampler.sample(  samples=1000,   dt=0.125  )
uMLDsampler.sample( samples=1000,   dt=0.125  )
MEQNsampler.sample(  samples=1000,   dt=0.125  )
uMEQNsampler.sample( samples=1000,   dt=0.125  )

if (master):
    print ""
    print "Burn-in completed. Now running 50k steps..."
    print ""

t0 = time.clock()
q,llh,a = MLDsampler.sample( samples=50000,  dt=0.125  )

if ( master ): 
    
    tauF, tauS, mf,ms,sigf,sigs = get_iat( q )
    print "Metropolized Langevin dynamics: "
    print "  > Acceptance ratio: " + str(np.mean(a))
    print "  > Fast mean: " + str(mf) + ", Slow mean: " + str(ms)
    print "  > Fast sigma: " + str(sigf) + ", Slow sigma: " + str(sigs)
    print "  > Fast tau: " + str( tauF ) + ", Slow tau: " + str( tauS )
    print "  > Wall time: " + str( time.clock() - t0 ) + "s"
    print ""

t0 = time.clock()
q,llh,a = uMLDsampler.sample( samples=50000,  dt=0.125, Metropolize=False  )

if ( master ): 
    
    tauF, tauS, mf,ms,sigf,sigs = get_iat( q )
    print "un-Metropolized Langevin dynamics: "
    print "  > Acceptance ratio: " + str(np.mean(a))
    print "  > Fast mean: " + str(mf) + ", Slow mean: " + str(ms)
    print "  > Fast sigma: " + str(sigf) + ", Slow sigma: " + str(sigs)
    print "  > Fast tau: " + str( tauF ) + ", Slow tau: " + str( tauS )
    print "  > Wall time: " + str( time.clock() - t0 ) + "s"
    print ""

t0 = time.clock()
q,llh,a = MEQNsampler.sample( samples=50000, comfreq=1000, dt=0.125, eta=25  )

if ( master ): 
    
    tauF, tauS, mf,ms,sigf,sigs = get_iat( q )
    print "Metropolized EQN dynamics: "
    print "  > Acceptance ratio: " + str(np.mean(a))
    print "  > Fast mean: " + str(mf) + ", Slow mean: " + str(ms)
    print "  > Fast sigma: " + str(sigf) + ", Slow sigma: " + str(sigs)
    print "  > Fast tau: " + str( tauF ) + ", Slow tau: " + str( tauS )
    print "  > Wall time: " + str( time.clock() - t0 ) + "s"
    print ""

t0 = time.clock()
q,llh,a = uMEQNsampler.sample( samples=50000, comfreq=1000,  dt=0.125, eta=10, Metropolize=False  )

if ( master ): 
    
    tauF, tauS, mf,ms,sigf,sigs = get_iat( q )
    print "un-Metropolized EQN dynamics: "
    print "  > Acceptance ratio: " + str(np.mean(a))
    print "  > Fast mean: " + str(mf) + ", Slow mean: " + str(ms)
    print "  > Fast sigma: " + str(sigf) + ", Slow sigma: " + str(sigs)
    print "  > Fast tau: " + str( tauF ) + ", Slow tau: " + str( tauS )
    print "  > Wall time: " + str( time.clock() - t0 ) + "s"
    print ""
    
 
    

