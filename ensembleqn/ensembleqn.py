import numpy as np
from numpy import random
from Lutils import getL
import time
import datetime

class Sampler:
    
    def __init__(self, lpf, glpf=None, nwalkers=0, ic=0, seed=1,r0=0.001, ngroups=0, maxits=10, maxeps=0.00001,debug=False):
        
        from mpi4py import MPI 

        self.comm = MPI.COMM_WORLD 
        self.rank = self.comm.Get_rank()
        self.size = self.comm.Get_size()
        self.debug = debug

        if (nwalkers < 2*self.size): 
            nwalkers = 2*self.size
            #if self.is_master():
            #    raise ValueError("You're recommended to use at least " + str( 2*self.size ) + " walkers!")
            #exit()

        self.lpf = lpf 
        
        if (glpf is None):
            self.glpf = self.zerofn
        else:
            self.glpf = glpf
        
        if (ngroups<2):
            self.ngroups = int( nwalkers / self.size )
        else:
            self.ngroups = ngroups
            
        self.nwalkers = int( nwalkers / (self.ngroups * self.size) ) * (self.ngroups * self.size)
        
        self.seed = seed + self.rank
        self.Random = random.RandomState( self.seed )
        
        resizeIC = True
        
        try:
            if (np.shape( ic)[0] >1 and np.shape( ic )[1] > 1):
                resizeIC = False
        except:
            resizeIC = True
            
            
        if (resizeIC):
            self.ic = np.array(ic).flatten() 
            
            self.dim = np.size( self.ic ) 
            
            self.ic = np.reshape( self.ic, (self.dim,1) )
            
            self.q = self.ic + r0*self.Random.randn( self.dim, self.nwalkers ) 
        else:
            if (np.shape(ic)[1]) < self.nwalkers:
                ic = np.tile( ic , ( 1, np.int( np.ceil(self.nwalkers/np.shape(ic)[1] )))   ) 
                
            self.ic = ic
            self.dim = np.shape( self.ic )[0] 
            
            self.q = np.copy( ic[:,:nwalkers] )
                
        
        self.v = np.array( [None] *  self.nwalkers )
        
        self.maxits = maxits
        self.maxeps = maxeps
        
        if self.is_master() and self.debug:
            print "MPISIZE: " + str(self.size)
            print "Number of groups: " + str(self.ngroups )
            print "Number of walkers: " + str( self.nwalkers ) 
             
    def zerofn(self,p):
        return 0
    
    def is_master(self): 
        return self.rank==0
    
    
    def sample(self, samples=1000, comfreq=0, steps=1, gamma=1.0, dt=0.01, Metropolize=True, eta=0, lam=0, localvars=None, showprogress=False ):
        
        traj_q = None
        traj_v = None
        traj_a = None
          
        self.q, self.v = self.syncdata( self.q, self.v)
        
        w = np.arange( self.nwalkers )
        gidx = [  w[g:self.nwalkers:self.ngroups]  for g in np.arange(self.ngroups) ]
        myidx = [ gidx[g][ self.rank::self.size ]  for g in np.arange(self.ngroups) ]
          
        Tq = np.zeros(( self.dim, self.nwalkers, samples )) 
        Tv = np.zeros(( 1 , self.nwalkers, samples ))
        Ta = np.zeros(( 1 , self.nwalkers, samples ))
        
        T = 0
        
        if comfreq<=0:
            comfreq = samples
        if comfreq>samples:
            comfreq = samples
        samples = samples - (samples % comfreq)
            
        starttime = time.time()
        
        while  (T<samples):
            
            g = 0
            while (g<self.ngroups):
                
                idx = myidx[ g ] # list of indices to run on
                
                gg = np.arange( self.ngroups )  
                notg = np.delete( gg , g )
                notglist = [] 
                
                for ng in notg:
                    notglist.extend( gidx[ ng ] ) 
                 
                
                v = self.v[ idx ]
                q = self.q[: , idx ]
                Q = self.q[: , notglist]  
                
                q,v, traj_q, traj_v, traj_a = self.run(q,v, Q , N=comfreq, steps=steps, gamma=gamma, dt=dt, Metropolize=Metropolize,eta=eta,lam=lam, localvars=localvars)
                
                Tq[:,idx, T:(T+comfreq)] = traj_q
                Tv[:,idx, T:(T+comfreq)] = traj_v
                Ta[:,idx, T:(T+comfreq)] = traj_a
                
                
                self.swapdata( q , v , idx )
                
                g+=1
                #run
                
                #trade data
                
            T += comfreq
        
            if (self.is_master() and showprogress):
                self.print_update( (1.0*T) / samples , time.time() - starttime , np.mean( traj_a ) )
         
        idxs = [int(ii) for ii in np.array( myidx[:] )]
         
        Tq = Tq[:, idxs, :]
        Tv = Tv[:,idxs,:]
        Ta = Ta[:,idxs,:]
        
        if (self.is_master() and self.debug):
            print "Now pooling trajectories..."
            
        Tq,Tv,Ta = self.pooldata( Tq,Tv,Ta, idxs )
        
        if (self.is_master() and self.debug):
            print "Completed!"
        
        return (Tq,Tv,Ta) 
        
        
        
    def run(self, q0 , v0 , Q , N, steps, gamma, dt, Metropolize=True, eta=0, lam=0, localvars=None):
        
        q = np.copy(q0)
        v = np.copy(v0)
        
        W = np.size(v)
        
        c1 = np.exp(-gamma*dt)
        c3 = np.sqrt( 1.0 - c1*c1 )
        
        dt2 = dt*0.5
        
        traj_q = np.zeros((self.dim , W , N ))
        traj_v = np.zeros(( 1 , W , N ))
        traj_a = np.zeros(( 1 , W , N ))
        
        L = None
        J1 = 1
        J2 = 1
        J = 1
        logJ = 0
        divL = 0
        dL = 0
        if (localvars is None):
            lv = np.arange(self.dim)
        else:
            lv = localvars
         
        
        for ii in np.arange(W):
            
            qq = np.reshape( q[:,ii] , (self.dim,1) )
            
            pp = self.Random.randn( self.dim, 1 )  
            ff = self.glpf( qq )
            
            
            vv = v[ii]
            if (vv is None):
                vv = self.lpf( qq )
                
            L = getL( qq , Q, eta, lam, L, lv, False )[0]
                
            for n in np.arange(N):
                
                ExtraProb = 0.0
                q0 = np.copy( qq )
                p0 = np.copy( pp )
                v0 = vv
                f0 = np.copy( ff )
                H0 = np.dot(p0.T,p0)*0.5 - vv 
                L0 = np.copy( L )
                accept = True
                
                
                
                    
                for step in np.arange(steps):
                    
                    # B 
                    pp = pp +  dt2* np.dot( np.transpose(L) , ff) 
                    
                    # A 
                    if (eta*lam <=0):
                        qq = qq + dt2 * np.dot(  (L) , pp)
                    else: # we have to do an implicit step
                        qnew = qq + dt2 * np.dot(  (L) , pp)
                        its = 0
                        eps = 1
                        Lnew = L
                        while ( (its < self.maxits) and (eps > self.maxeps) ):
                            Lnew = getL( qnew , Q, eta , lam, Lnew, lv, ComputeDiv=False  )[0]
                            qnew_prev = qnew
                            qnew = qq+dt2*np.dot(  (Lnew) , pp)
                            eps = np.sum( np.abs( qnew - qnew_prev ) )
                            its = its + 1
                        if ((its >= self.maxits)):  
                            accept = False
                            break
                            
                        
                        qq = np.copy(qnew)
                        L, divL, dL = getL( qq , Q, eta, lam, L, lv ) 
            
                        # (Jacobian correction)  
                        dLp = np.zeros( (self.dim,self.dim) )
                        #print np.shape(divL) 
                        xx = np.dot(dL,pp)
                        dLp[lv,:] = np.squeeze(xx)
                        J2 = np.linalg.det( np.eye(self.dim) + dt2*dLp )
                        
                    # B_div
                    pp = pp + dt2*divL
                    
                    # O 
                    p_pre = pp
                    R = c3* self.Random.randn( self.dim, 1 )
                    pp = c1*pp + R
                    p_post = pp
                    
                    # B_div
                    pp = pp + dt2*divL
                    
                    
                    # (Jacobian correction part two)
                    
                    if (eta*lam >0):  
                        xx = np.dot(dL,pp)
                        dLp[lv,:] = np.squeeze(xx)
                        J1 = np.linalg.det( np.eye(self.dim) - dt2*dLp )
                    
                        J = J2/J1
                    
                        if (J>0 and not np.isinf(J) and not np.isnan(J) ):
                            logJ = np.log(J)
                        else:
                            logJ = -np.inf
            
            
                    # A  
                    qq = qq + dt2 * np.dot(  (L) , pp)
                    
                    try:
                        ff = self.glpf(qq)
                    except:
                        ff=np.nan
                        
                    if ((np.sum( np.isnan( ff ) )>0 ) or (np.sum( np.isinf( ff ) )>0 ) ):
                        accept = False
                        break
                    
                    L  = getL( qq , Q, eta, lam, L, lv ,ComputeDiv=False  )[0] 
                    
                    # B  
                    pp = pp + dt2*np.dot( np.transpose(L) , ff)
                    
                    
                    
                    p1 = np.sum( R*R ) / (2*c3*c3)
                    p2 = c1 * p_post - p_pre
                    p2 = np.sum( p2*p2 ) / (2*c3*c3)

                    ExtraProb += p2-p1-logJ
                
                if (accept):
                    vv = self.lpf(qq)
                else:
                    vv = 0 
                
                if (np.isinf(vv) or np.isnan(vv) ):
                    accept = False
                
                if (Metropolize):
                    H = np.dot(pp.T,pp)*0.5 - vv

                    u = np.log( self.Random.rand() ) 
                    dH = H0-H-ExtraProb
                    if (u>dH):
                        accept = False 
                    
                if (accept):
                    aa = 1
                else:
                    aa = 0
                    qq=q0
                    vv=v0
                    pp=-p0
                    ff=f0
                    L=L0
                    
                     
                traj_q[:,ii,n] = np.squeeze( qq )
                traj_v[:,ii,n] = np.copy(vv)
                traj_a[:,ii,n] = np.copy(aa)
            
            q[:,ii] = np.squeeze( qq )
            v[ii] = vv
            
         
        return q,v, traj_q, traj_v, traj_a
    
    
    def syncdata(self, q , v ):

        newq = np.zeros( np.shape(q) )
        newv = np.zeros( np.shape(v) )
  
        newq = self.comm.bcast( q, root=0 )
        newv = self.comm.bcast( v, root=0 ) 
            
        return newq, newv
        
        
    def swapdata(self, q , v, idx ):
  
        XX = self.comm.allgather( [q , v, idx] )
         
        for q,v,xid in XX:
            self.q[:,xid] = q
            self.v[xid] = v
            
        return 
        
        
    def pooldata(self, Tq,Tv,Ta, idx ):
  
        samples = np.shape(Tq)[2]
        
        XX = self.comm.gather( [Tq,Tv,Ta,idx], root=0 )
         
        if self.is_master():
            Tq = np.zeros((self.dim , self.nwalkers , samples ))
            Tv = np.zeros(( 1 , self.nwalkers , samples ))
            Ta = np.zeros(( 1 , self.nwalkers , samples ))
            for tq,tv,ta,idxs in XX:
                Tq[:,idxs,:] = tq
                Tv[:,idxs,:] = tv
                Ta[:,idxs,:] = ta 
            
        else:
            Tq = None
            Tv = None
            Ta = None
            
        return (Tq,Tv,Ta)

    def print_update(self, prog , tt ,  acc  ):
        
        myprog = prog*100.0
        
        if prog>0:
            tr = tt * ( 1.0-prog ) / prog
        else:
            tr = 0
        
        acp = acc * 100.0
        
        
        timetaken = str(datetime.timedelta(seconds=int(tt)))
        timeleft = str(datetime.timedelta(seconds=int(tr)))
        
        print "####"
        print("# Progress: %3.1f%%    (Time: %s,   Remaining: %s)"%(myprog, timetaken,timeleft))
        print("# Acceptance  %3.2f%% "%(acp) )
    