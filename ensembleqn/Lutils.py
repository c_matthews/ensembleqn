import numpy as np

def getL(x , X ,  eta, lam, L, lv,ComputeDiv=True):
    
    if (eta<=0): # Just vanilla LD
        return np.array(1),0,0
    
    
    if (lam<=0): # Use global cov
        if (L is not None): 
            return L,0,0 # Reuse global cov
    
    
    w,wp,mu,mup,X  = getW( x , X, lam, lv, ComputeDiv ) 
    
    
    dim = np.size(x)
    
    v1 = np.sum(w)
    wn = w / v1
    XX = X.T - np.sum( X*wn , axis=1)
    C = np.dot( XX.T * wn , XX )
 
    try:
        L = np.linalg.cholesky( np.eye( dim ) + (eta) * C ) 
    except:
        L = np.eye( dim )

    divL = np.zeros( (dim,1) )
    dL = [ np.zeros( (dim,dim) )   for _ in np.arange(0,len(lv)) ]
 
    if ((lam <= 0) or (not ComputeDiv) ):
        return L, divL, dL
    
    
    pC = C
    iL = np.linalg.inv( L )
    #iL = np.linalg.solve( L , np.eye( 9 ) )
    
    #print iL
    #return np.eye(self.dim), np.zeros( self.dim ), np.zeros( (self.dim,self.dim) )
    
    #wn = w / np.sum(w)
    wn2 = wn*2
    M1 = XX.T * wn2
    #return np.eye(self.dim), np.zeros( self.dim ), np.zeros( (self.dim,self.dim) )
    
    for idd,ii in enumerate(lv): 
        nw = wp[:,ii] / v1
        
        C = XX
        C1 = np.dot( C.T , (C.T * nw).T ) 
        
        M2 = mup[:,ii] 
        M2 = np.tile( M2 , (M1.shape[1],1)) 
        C2 = -np.dot( M1,M2)  
        
        C3 = -pC * np.sum( nw ) 
        
        dC = eta*( C1+C2+C3 )
        
        z = np.dot( dC , iL.T )
        z = np.dot( iL , z )
        z = np.tril(z ) - 0.5*np.diag(np.diag( z ) )
        dLdi = np.dot( L , z ) 

        #print (ii, np.shape(dL), np.shape(dLdi))
        #dL.append( dLdi[:,:,id] =  dLdi

        dL[idd] += dLdi
        
        dLdii = np.reshape( dLdi[ii,:] , (dim,1) )
        #print dLdii
        #print divL
        
        divL = divL + dLdii
         
    
    return L,divL, dL
    

def getW(x , X, lam, lv, ComputeDiv):
    
    wp = 0
    mu = 0
    mup = 0
    
    dim = len(x)
    
    if (lam<=0):
        return np.ones( np.shape(X)[1] ),wp,mu,mup,X 
        
    xx = x[ lv ]
    XX = X[ lv , : ]
    
    dX = XX.T - np.squeeze(xx)

    # TODO divide by inv cov
    
    # C = np.cov( XX )
    
    invCdX = dX.T  # np.linalg.solve( C , dX.T )
    
    dw = np.sum( dX * invCdX.T, axis=1 )
    dw = dw - np.min(dw)
    
    #print dw
    ww = - (0.5*lam) * dw

    kk = ww >-7 #0.001
    ww=ww[kk]
    X=X[:,kk]
    w=np.exp( ww  )
    dX=dX[kk,:]

    #w = np.exp( - (0.5*self.locality) * dw  )
    mw = np.max(w)
        
    w = w / mw    

    
    if ((not ComputeDiv) ):
        return w,wp,mu,mup,X 
    
    sw = np.sum(w)
    wn = w / sw
    
    wp = np.zeros( (len(w), dim ) )
    
    mup = np.zeros( (dim,dim) )
    
    temp = (lam*w) * dX.T
    wp[:,lv ] = temp.T
    wpn = wp / sw
    
    #for ii in self.localvars:
    #    mup[:,ii] = np.sum( wpn[:,ii] * X, axis=1  ) # / sw

    mup[:,lv] = np.dot( X , wpn[:,lv ] )

    #print mup[:,self.localvars]

    #xx=  np.dot( X , (wpn[:,self.localvars ])  )
    #xx = np.sum(  X.T  *   wpn[:,self.localvars] , axis=1)
    #print xx

    #exit()
    
    swpn = np.sum( wp, axis=0) / sw
    mu = np.sum( X*wn , axis=1) # / np.sum(w)
    
    mup = mup - np.outer( mu , swpn ) #/ sw
    
    
    
    
    return w,wp,mu,mup,X