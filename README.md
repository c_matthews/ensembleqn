Ensemble Quasi-Newton
==============

This package is a Python implementation of the EQN method found in https://arxiv.org/abs/1607.03954
This is a sampling algorithm that automatically scales dynamics in order to compensate for badly conditioned distributions.

Installation
--------------

Simply copy the files into a directory and run. There is also a setup.py file for use with pip install.

Requirements
--------------

The examples and package make use of the following commonplace python modules:

* Python 2.7+
* Numpy 1.6+
* Mpi4py 
* Acor


Usage
--------------

Once you have a log likelihood function defined (and possibly a derivative of this function defined), you are able to use this package. First you must

    import ensembleqn

in order to access the package. Next you need to set up the sampler, using a call like 

    sampler = ensembleqn.Sampler( lpf=log_prob_fn , glpf=grad_log_prob_fn, nwalkers=64, ic=[0,0,0,0]  )

This will create *nwalkers* copies of the initial condition, slightly perturbed. The sampler object will keep track of their current position. You can evolve the ensemble by calling the Sample routine

    q,llh,a = sampler.sample( samples=1000,  dt=0.125  )
    
The output is the positions *q*, the log likelihood at each position *llh*, and whether that point was accepted or rejected *a*. The q vector has dimensionality

    numpy.shape(q) =  ( Dimensions ,  nwalkers  ,  nsamples  )

So running for 250 samples on a 2d system with 100 walkers, q is a 2x100x250 array. The llh and a arrays would be 1x100x250, as they only output a scalar value every step.

The examples use the ACOR package on the ensemble average of the data. This returns an approximation to the integrated autocorrelation time (IAT), which gives an estimate for how many samples are needed before we recover a new sample. An efficient sampler will minimize the maximum IAT. We hope that the EQN sampler does this!

The full list of options for the sampler are:
~~~~
    Sampler = ensembleqn.Sampler( lpf, glpf, nwalkers, ic, seed,r0, ngroups, maxits, maxeps,debug )
~~~~
where:

*       `lpf` : [**Required**] This is the function for evaluating the log probability function, taking a single argument.
*       `glpf`: [Default: zero-function] The function evaluating the gradient of the log probability function, taking a single argument. If not given, then no gradient information will be used in the simulation, and the dynamics will be like a random walk with intertial memory.
*       `nwalkers` : [Default: 2xCores] The number of walkers that will be used in the simulation. The minimum number (and the default number) is twice the number of available processes available when you run `mpirun`.
*       `ic` : [Default: 0] The initial conditions to being using the walkers, of dimension  [D,K] where D is the dimensionality of the problem, and there are K different initial conditions to split between the walkers
*       `seed` : [Default: 1] The seed to use for the random number generator.
*       `r0` : [Default: 0.001] The walkers are initialized at points that are in a normal ball around `ic`, with radius `r0`
*       `ngroups` : [Default: `nwalkers` / Cores] The number of groups to run with.
*       `maxits` : [Default: 10 ] The maximum number of iterations to use in the fixed point iteration of the EQN sampler. Reducing this number may improve performance.
*       `maxeps` : [Default: 0.00001] The convergence tolerance for the fixed point iteration
*       `debug` : [Default: False] Displays some debug information if set to *True*

Once the sampler is created, you can run the sample routine using
~~~~
    q,llh,a = Sampler.sample( samples, comfreq, steps, gamma, dt, Metropolize, eta, lam, localvars, showprogress )
~~~~
where: 

+       `samples`: [Default: 1000] The total number of samples to output.
+       `comfreq` : [Default: `samples`] The frequency which samples are communicated, and the frequency the momentum is reset.
+       `steps` : [Default: 1] The number of steps to use in one sample.
+       `gamma` : [Default: 1] The friction parameter to use in the dynamics. Must be positive.
+       `dt` : [Default: 0.01] The time step to use.
+       `Metropolize` : [Default: True] Whether to Metropolize the dynamics or not. Setting to *False* will accept every step where the likelihood function does not return a nan-value.
+       `eta` : [Default: 0 ] The strength of the boost provided by the EQN. Set to zero to run Langevin dynamics, otherwise a positive number will run EQN dynamics.
+       `lam` : [Default: 0] The squared length scale to use to define the closeness of samples
+       `localvars` : [Default: all variables] A list of variables to use in the distance calculation
+       `showprogress` : [Default: False] Displays information on the progress of the job, every `comfreq` samples, if set to *True*



Examples
--------------

You can run one of the examples by executing

    mpirun -n N python exampleX.py

where *N* is the number of cores to use. 

Experimenting these files will give some insight on how to use the package. There are some interesting parameters which the user may want to experiment with (see the usage section), such as modifying the number of walkers, and the frequency of communication, changing the number of groups, modifying the timestep, removing the derivative function or changing  the *eta* parameter.

There are two examples:

##### example1.py : 1

![alt text](images/ex1.png "Example 1 distribution")
 
Compares schemes sampling a 2d Gaussian distribution whose covariance matrix has eigenvalues 1 and 99, so trajectories will sample one direction slower than the other. EQN uses the global covariance matrix of the walkers to precondition the dynamics to remove this timescale disparity, recovering relatively even sampling. The example uses Metropolized and UnMetropolized dynamics.  This example does not take long to run, example output looks like:
~~~~
    Metropolized Langevin dynamics: 
    > Acceptance ratio: 0.846356875
    > Fast mean: 0.249984289227, Slow mean: 0.748360133635
    > Fast sigma: 0.000107394540956, Slow sigma: 0.00761911753753
    > Fast tau: 0.921777787727, Slow tau: 45.5716530682
    > Wall time: 17.52s

    un-Metropolized Langevin dynamics: 
    > Acceptance ratio: 1.0
    > Fast mean: 0.249979664109, Slow mean: 0.756902420091
    > Fast sigma: 0.000113238311848, Slow sigma: 0.00439376479638
    > Fast tau: 1.00020806997, Slow tau: 16.1124120426
    > Wall time: 15.59s

    Metropolized EQN dynamics: 
    > Acceptance ratio: 0.790681875
    > Fast mean: 0.249973943196, Slow mean: 0.74836929044
    > Fast sigma: 0.000113755312077, Slow sigma: 0.00224525746018
    > Fast tau: 1.03592319406, Slow tau: 3.95854291481
    > Wall time: 17.44s

    un-Metropolized EQN dynamics: 
    > Acceptance ratio: 1.0
    > Fast mean: 0.249955546971, Slow mean: 0.751424044345
    > Fast sigma: 9.47652696142e-05, Slow sigma: 0.00106191441029
    > Fast tau: 0.724718351115, Slow tau: 0.889791318716
    > Wall time: 15.55s 
~~~~

##### example2.py : Hidalgo stamps

![alt text](images/ex3.png "Example 2 distribution")

Compares schemes on the Gaussian mixture (Hidalgo stamps) model. Due to label permutation this model has six identical basins that are poorly scaled in different directions, due to the switching of the variables. We use 64 pre-initialized walkers that are scattered in each of the six wells, and use the local covariance matrix to determine the scaling within a well. The local weights are decided by using the L2 distance in only three of the nine dimensions in the space. We give the autocorrelation time relative to the number of force evaluations required. Even though this is a small model, we find a huge improvement in sampling efficiency using the EQN package.
~~~~
    Metropolized Langevin dynamics: 
    > Wall time: 2329.05s
    > Acceptance ratio: 0.88906921875 (Max: 0.897905, Min: 0.872335)
    > Weights mean: 0.229467667499, Precision mean: 37.5281560535, Center mean: 7.16887274818, Beta mean: 0.110613845762
    > Weights sigma: 0.000808602972743, Precision sigma: 0.268287167472, Center sigma: 0.00116959639441, Beta sigma: 0.000454878109358
    > Weights tau: 25765.4501311, Precision tau: 27087.5196364, Center tau: 28712.2565537, Beta tau: 5171.25049472
  
    Metropolized EQN dynamics: 
    > Wall time: 10430.19s
    > Acceptance ratio: 0.859144765625 (Max: 0.87, Min: 0.847895)
    > Weights mean: 0.227287430921, Precision mean: 38.2990415333, Center mean: 7.16579134106, Beta mean: 0.109591295993
    > Weights sigma: 3.78398803989e-05, Precision sigma: 0.0147441367404, Center sigma: 5.53467673195e-05, Beta sigma: 3.92952980638e-05
    > Weights tau: 82.838536428, Precision tau: 90.8004349465, Center tau: 103.557064536, Beta tau: 40.7525150521
~~~~
Due to the simplicity of the model, the dominant cost in time is still the linear algebra required in the EQN scheme. However, even with this cost the improvement made is stark (a factor of 100 or so).


Known issues
--------------

The ACOR package will sometimes give an error due to an insufficient trajectory length, or sometimes when the *tau* value is very small.


Help
---------------

Please sent any suggestions, bugs and difficulties to c.matthews@uchicago.edu
