#
# Samples the Hidalgo stamps problem using EnsembleQN sampling  
# 
import ensembleqn.ensembleqn as eqn
import numpy as np
import acor  
import time
from mpi4py import MPI
from likelihoods.gmix import getllh, gradllh

if (MPI.COMM_WORLD.Get_rank()==0):
    master=True
else:
    master=False

global y
global ic
# Load the training stamp thickness data
y = np.loadtxt("likelihoods/stampdata.dat")

# We load some pre-equilibrated initial conditions
ic = np.loadtxt("likelihoods/stamp_ic.dat")

def myllh(p):
    
    return getllh(p,y,3,2,0.2,4,100)

def mygllh(p):
    
    return gradllh(p,y,3,2,0.2,4,100)

#
# Define the function that gets the fast and slow IATs using ACOR
#
def get_iat( q ): 
     
    minweights = np.squeeze( q[0,:,:] ) 
    minweights = np.fmin(minweights, np.squeeze( q[1,:,:] )  )
    minweights = np.fmin(minweights, 1 - (np.squeeze( q[0,:,:] ) + np.squeeze( q[1,:,:] )  ) )
    
    maxlambda = np.squeeze( q[2,:,:] ) 
    maxlambda = np.fmax( maxlambda , np.squeeze( q[3,:,:] ) )
    maxlambda = np.fmax( maxlambda , np.squeeze( q[4,:,:] ) )
    
    minmu = np.squeeze( q[5,:,:] ) 
    minmu = np.fmin( minmu , np.squeeze( q[6,:,:] ) )
    minmu = np.fmin( minmu , np.squeeze( q[7,:,:] ) )
    
    beta = np.squeeze( q[8,:,:] )
    
    qweights = np.mean( minweights,axis=0 )
    qlambda = np.mean( maxlambda,axis=0 )  
    qmu = np.mean( minmu,axis=0 )  
    qbeta = np.mean( beta,axis=0 )  

    try:
        t1, m1, s1 = acor.acor( qweights )
    except:
        t1, m1, s1 = 1.0,0,0 # Error!
        
    try:
        t2, m2, s2 = acor.acor( qlambda )
    except:
        t2, m2, s2 = 1.0,0,0 # Error!
        
    try:
        t3, m3, s3 = acor.acor( qmu )
    except:
        t3, m3, s3 = 1.0,0,0 # Error!
        
    try:
        t4, m4, s4 = acor.acor( qbeta )
    except:
        t4, m4, s4 = 1.0,0,0 # Error!
    
    return [t1,t2,t3,t4],[m1,m2,m3,m4],[s1,s2,s3,s4]
#
# Now create the  sampler objects
# We use 64 walkers in this example 
#
if (master):
    print "Creating sampling objects..."

if (master):
    print " > Metropolized Langevin dynamics"
LDsampler = eqn.Sampler( myllh , mygllh , nwalkers=64, ic=ic  ) 

if (master):
    print " > Metropolized EQN dynamics"
EQNsampler = eqn.Sampler( myllh , mygllh , nwalkers=64, ic=ic  ) 

if (master):
    print ""
    print "Doing burn-in (1000steps)..."
LDsampler.sample( samples=1000, dt=0.015, gamma=0.01  )
EQNsampler.sample( samples=1000, dt=0.015, gamma=0.01  )


if (master):
    print ""
    print "Running..."
    print ""

t0 = time.clock()
q,llh,a = LDsampler.sample( samples=200000, comfreq=1000, dt=0.015,  steps=5    )

if ( master ): 
    
    t,m,s = get_iat( q )
    t = np.array(t)*5
    aa = np.squeeze( np.mean( a , axis=2 ) )

    print "Metropolized Langevin dynamics: "
    print "  > Wall time: " + str( time.clock() - t0 ) + "s"
    print "  > Acceptance ratio: " + str(np.mean(a)) + " (Max: " + str(np.max(aa)) + ", Min: " + str(np.min(aa)) + ")"
    print "  > Weights mean: " + str(m[0]) + ", Precision mean: " + str(m[1]) + ", Center mean: " + str(m[2]) + ", Beta mean: " + str(m[3])
    print "  > Weights sigma: " + str(s[0]) + ", Precision sigma: " + str(s[1]) + ", Center sigma: " + str(s[2]) + ", Beta sigma: " + str(s[3]) 
    print "  > Weights tau: " + str(t[0]) + ", Precision tau: " + str(t[1]) + ", Center tau: " + str(t[2]) + ", Beta tau: " + str(t[3]) 
    print ""  
        
t0 = time.clock()
q,llh,a = EQNsampler.sample( samples=200000, comfreq=1000, dt=0.015, steps=5, eta=100, lam=12, localvars=[5,6,7]    )

if ( master ): 
    
    t,m,s = get_iat( q )
    t = np.array(t)*5
    aa = np.squeeze( np.mean( a , axis=2 ) )

    print "Metropolized EQN dynamics: "
    print "  > Wall time: " + str( time.clock() - t0 ) + "s"
    print "  > Acceptance ratio: " + str(np.mean(a)) + " (Max: " + str(np.max(aa)) + ", Min: " + str(np.min(aa)) + ")"
    print "  > Weights mean: " + str(m[0]) + ", Precision mean: " + str(m[1]) + ", Center mean: " + str(m[2]) + ", Beta mean: " + str(m[3])
    print "  > Weights sigma: " + str(s[0]) + ", Precision sigma: " + str(s[1]) + ", Center sigma: " + str(s[2]) + ", Beta sigma: " + str(s[3]) 
    print "  > Weights tau: " + str(t[0]) + ", Precision tau: " + str(t[1]) + ", Center tau: " + str(t[2]) + ", Beta tau: " + str(t[3]) 
    print ""   
        