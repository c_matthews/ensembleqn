import numpy as np

global iw
global h
iw = 1.0/ 20.0
h  = 100.0   
        
def getllh(p ):
    
    X = p[0]
    Y = p[1]

    V1 = ( Y - X*X )
    V2 = ( 1.0 - X )

    V = - ( h * V1*V1  +  V2*V2  ) * iw
    
    return V

def gradllh(p):

    F = np.zeros( np.shape(p) ) 
    
    X = p[0]
    Y = p[1]

    V1 = ( Y - X*X )
    V2 = ( 1.0 - X )
    

    F[0] = iw * ( 2*h* V1 * 2 * X  +  2 * V2 )
    F[1] = -iw * ( 2 *h * V1 )
    
    return F
 
        
