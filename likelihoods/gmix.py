import numpy as np
    
def getllh( x , y, N, alpha, g, kappa, h ):
 

    R = np.max(y) - np.min(y)

    kappa = kappa / ( R*R )
    h = h * g / (alpha * R * R )

    M = np.mean(y)

    qk = np.zeros((N,1))
    
    qk[0:N-1] = x[0:N-1]
    
    qk[-1] = 1.0-np.sum(qk)

    lambdak = x[N-1:2*N-1]
    muk = x[2*N-1:3*N-1]
    beta = x[-1]

    neg = 0
    neg += (qk<0).sum()
    neg += (lambdak<0).sum()
    neg += (beta<0).sum()

    if (neg>0):
        return np.nan
    
    YminusMu = np.tile( y , (N,1) ).transpose() - np.squeeze(muk)
    xp = np.exp( -0.5*np.squeeze( lambdak ) * np.power( YminusMu ,2) )
    slambdak = np.sqrt( np.squeeze( lambdak ) ) 
    F = xp * (np.squeeze( qk ) * slambdak)


    Fsum = np.sum(F,axis=1)
    FF=F

    
    V = (N * alpha +g -1) * np.log(beta) + (alpha-1) * np.sum( np.log( lambdak) )
    V = V - kappa*0.5*np.sum( np.power(muk - M,2) )
    V = V - beta * (h + np.sum(lambdak))
    
    X = np.log( np.sum(FF,axis=1) );
    
    X = np.sum(X);
    V = V + X;
    
    return V

def gradllh( x , y, N, alpha, g, kappa, h ): 
    
    R = np.max(y) - np.min(y)

    kappa = kappa / ( R*R )
    h = h * g / (alpha * R * R )

    M = np.mean(y)

    qk = np.zeros((N,1))
    
    qk[0:N-1] = x[0:N-1]
    
    qk[-1] = 1.0-np.sum(qk)

    lambdak = np.squeeze( x[N-1:2*N-1] )
    muk = np.squeeze( x[2*N-1:3*N-1] )
    beta = np.squeeze( x[-1] )

    neg = 0
    neg += (qk<0).sum()
    neg += (lambdak<0).sum()
    neg += (beta<0).sum()

    if (neg>0):
        return np.nan * np.ones( np.shape(x) )

    Vbeta = ( N * alpha + g - 1 ) / beta - ( h + np.sum(lambdak) )
    slambdak = np.sqrt( np.squeeze(lambdak) ) 
    YminusMu = np.tile( y , (N,1) ).transpose() - np.squeeze(muk)
    xp = np.exp( -0.5*np.squeeze(lambdak) * np.power( YminusMu ,2) )
    F = xp * (np.squeeze( qk ) * slambdak)
 
    Fsum = np.sum(F,axis=1)

    X = 0.5 * np.squeeze(qk) / slambdak
    X = X - 0.5*np.squeeze(qk) * np.squeeze(slambdak)*np.power( YminusMu ,2 )
    X = X * xp
    X = X.transpose() / Fsum

    #print np.shape(X)
    Vlambda = (alpha -1) / lambdak - beta + np.sum(X,axis=1 )
    #print np.shape( (alpha -1) / lambdak ), np.shape( beta ), np.shape( np.sum(X,axis=1 )) 
    X = slambdak * xp
    X = np.transpose(X[:,0:-1]) - X[:,-1]
    X = X / Fsum

    Vq = np.sum(X,axis=1)

    #X =  -Q.* sLAMBDA.*LAMBDA.*(MU-Y).* xp ./ Fsum;
    #Vmu = -kappa*(muk-M) + sum(X')';

    X = np.squeeze(qk) * slambdak * np.squeeze(lambdak)*YminusMu 
    X = X * xp
    X = X.transpose() / Fsum

    Vmu = -kappa*( muk - M) + np.sum(X,axis=1)
    
    
    #print np.shape( Vq ), np.shape( Vlambda ), np.shape( Vmu) 
    F = np.zeros( np.shape(x) )
    F[0:N-1] = np.reshape( Vq , (N-1,1) )
    F[N-1:2*N-1] = np.reshape( Vlambda, (N,1) )
    F[2*N-1:3*N-1] = np.reshape( Vmu, (N,1) )
    F[-1] = Vbeta
    

    
    return F

        
        
