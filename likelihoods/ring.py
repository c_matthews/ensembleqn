import numpy as np

global width
global amp
global freq 
width = 0.025
amp  = 0.0  
freq = 0.0
        
def getllh( x ):
    
    iwidth = 1.0 / width
    
    if ((x[0]*x[0] > 100) or (x[1]*x[1]>100) ):
        return [np.nan ]
        
    r2 = x[0]*x[0] + x[1]*x[1]
    V1 = ( r2 - 1.0) * iwidth
    if (amp>0):
        theta = np.arctan2( x[1],x[0] )
        V2 = np.cos( freq * theta )
    else:
        V2 = 0

    V = -V1*V1 - amp * V2*V2 

    return V 

def gradllh(x):
    
    iwidth = 1.0 / width
    
    if (not np.isfinite(x[0]) or not np.isfinite(x[1]) ):
        return [np.nan,np.nan]
    if ((x[0]*x[0] > 100) or (x[1]*x[1]>100) ):
        return [np.nan,np.nan]
    
    r2 = x[0]*x[0] + x[1]*x[1]
        
    V1 = (r2 - 1.0) * iwidth
  
    F = np.zeros( np.size(x) ) 

    F = (-4*iwidth*V1)*x
    if (amp>0):
        theta = np.arctan2( x[1],x[0] )
        V2 = np.cos( freq * theta )
    
        tmp = amp *freq*np.sin( 2*freq * theta ) / r2

        F[0] -= tmp * x[1]
        F[1] += tmp * x[0]

    return F
 
        
 